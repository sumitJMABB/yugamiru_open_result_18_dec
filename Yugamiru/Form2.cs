﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.IO;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public partial class Form2 : Form
    {
        ImageClipWnd m_ImageClipWnd = new ImageClipWnd();
        public Form2()
        {
            InitializeComponent();
            
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {

            /*
                        stretchDIBbits.BITMAPINFO bi = new stretchDIBbits.BITMAPINFO();
                        bi.bmiHeader.biWidth = 256;

                        bi.bmiHeader.biHeight = -96;

                        bi.bmiHeader.biSize = 40;

                        bi.bmiHeader.biBitCount = 16;

                        bi.bmiHeader.biPlanes = 1;



                        Random r = new Random();



                        // ushort[,] data = new ushort[96, 256];
                        byte[,] data = new byte[96, 256];

                        lock (r)
                        {

                            for (int i = 0; i < 96; i++)
                            {

                                for (int j = 0; j < 256; j++)
                                {

                                    data[i, j] = (byte)r.Next(0, 0xFFFF);

                                }

                            }

                        }



                        for (int i = 0; i < 60; i++)
                        {

                          stretchDIBbits.StretchDIBits(e.Graphics.GetHdc(), 0, 0, 256, 96, 0, 0, 256, 96,

                                data[i,j], ref bi, 0, Constants.SRCCOPY);

                        }*/


            //Image<Bgr, byte> test = new Image<Bgr, byte>(Yugamiru.Properties.Resources.sokui);
            //Image test = Yugamiru.Properties.Resources.sokui;

            //Image test = Image.FromFile(@"C:\Users\Meena\Desktop\pictures\flower.jpg");
            //Bitmap test = ConvertToBitmap(@"C:\Users\Meena\Desktop\pictures\sokui.jpg");
            

            //test.Save("C:\\Users\\Meena\\Desktop\\pictures\\flower.bmp", ImageFormat.Bmp);
            Image test = Image.FromFile(@"C:\Users\Meena\Desktop\suhana\20170217_121021.jpg");
            Image<Bgr,byte> emgucvimage = new Image<Bgr, byte>(Properties.Resources.kutui);
            //emgucvimage = CvInvoke.cvLoadImage(Properties.Resources.sokui.UnlockBits, Emgu.CV.CvEnum.LOAD_IMAGE_TYPE.CV_LOAD_IMAGE_COLOR);

             emgucvimage = emgucvimage.Resize(384, 480, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            //engucvimage = CvInvoke.cvLoadImage(@"C:\Users\Meena\Desktop\pictures\sokui.bmp",Emgu.CV.CvEnum.LOAD_IMAGE_TYPE.CV_LOAD_IMAGE_COLOR);
            byte[] emgucv_bytes = new byte[emgucvimage.Width * emgucvimage.Height * emgucvimage.MIplImage.nChannels];
            //byte[] emgucv_bytes = new byte[1024*1280* emgucvimage.MIplImage.nChannels];
            emgucv_bytes = emgucvimage.Bytes;
            //byte[] pByteBits = new byte[test.Bytes.Count()];
            byte[] pByteBits = converterDemo(test);
            stretchDIBbits.BITMAPINFO bmi = new stretchDIBbits.BITMAPINFO();
            bmi.bmiHeader.biSize = 40;//new stretchDIBbits.BITMAPINFOHEADER().biSize;
            bmi.bmiHeader.biWidth = emgucvimage.Width;//1024;//test.Width;
            bmi.bmiHeader.biHeight = -emgucvimage.Height;//-1280;//-test.Height;
            bmi.bmiHeader.biPlanes = 1;
            bmi.bmiHeader.biBitCount = 24;
            //bmi.bmiHeader.biCompression = stretchDIBbits.BitmapCompressionMode.BI_RGB;
            //bmi.bmiHeader.biCompression = stretchDIBbits.BitmapCompressionMode.BI_JPEG;
            bmi.bmiHeader.biSizeImage = 0;
            bmi.bmiHeader.biXPelsPerMeter = 0;
            bmi.bmiHeader.biYPelsPerMeter = 0;
            bmi.bmiHeader.biClrUsed = 0;
            bmi.bmiHeader.biClrImportant = 0;

            bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            bmi.bmiColors[0].rgbBlue = 255;
            bmi.bmiColors[0].rgbGreen = 255;
            bmi.bmiColors[0].rgbRed = 255;
            bmi.bmiColors[0].rgbReserved = 255;
            stretchDIBbits.SetStretchBltMode(e.Graphics.GetHdc(), StretchBltMode.STRETCH_HALFTONE);
            e.Graphics.ReleaseHdc();
                        var t = stretchDIBbits.StretchDIBits(
                                        e.Graphics.GetHdc(),
                                        0,//m_ImageClipWnd.m_iDestRectUpperLeftCornerX,
                                        0,//m_ImageClipWnd.m_iDestRectUpperLeftCornerY,
                                        200,//emgucvimage.Width,//test.Width,//m_ImageClipWnd.m_iDestRectWidth,
                                        200,//emgucvimage.Height,//test.Height,//m_ImageClipWnd.m_iDestRectHeight,
                                        0,
                                        0,//(m_ImageClipWnd.m_iBackgroundHeight - 1 - (0 + m_ImageClipWnd.m_iBackgroundHeight - 1)),
                                          emgucvimage.Width,//test.Width,//m_ImageClipWnd.m_iBackgroundWidth,
                                          emgucvimage.Height,//test.Height,//m_ImageClipWnd.m_iBackgroundHeight,
                                                             //test.Bytes,
                                        emgucv_bytes,//pByteBits,
                                        ref bmi,
                                        Constants.DIB_RGB_COLORS,
                                        Constants.SRCCOPY);

            e.Graphics.ReleaseHdc();
            /*
                        //get bmp from a file
                        Bitmap bmp = (System.Drawing.Bitmap)Image.FromFile(@"C:\Users\Meena\Desktop\pictures\sokui.bmp");
                        int width = bmp.Width;
                        int height = bmp.Height;

                        //get handle to bitmap
                         IntPtr hbmp = bmp.GetHbitmap();

                        //get handle to bitmap from bytes
                        //IntPtr hbmp = new Bitmap(new MemoryStream(emgucv_bytes)).GetHbitmap();



                        //get handle to source graphic
                        //get handle to source graphic
                        //IntPtr srcHdc = CreateCompatibleDC(e.Graphics.GetHdc());
                        e.Graphics.ReleaseHdc();
                        IntPtr bits0 = bmp.GetHbitmap();
                        IntPtr hbm0 = CreateDIBSection
                            (IntPtr.Zero, ref bmi, Constants.DIB_RGB_COLORS, out bits0, IntPtr.Zero, 0);
                        //bits0 = bmp.GetHbitmap();
                        //get original object and select bitmap
                        IntPtr pOrig = SelectObject(srcHdc, hbm0);
                      Graphics newGraphics = Graphics.FromHwnd(srcHdc);
                        Pen penNew = new Pen(Color.Red);
                        newGraphics.DrawLine(penNew, 500, 500, 600, 600);

                       // hbm0 = hbmp;
                        //bits0 = hbmp;
                        //copy it
                        bool res = StretchBlt(e.Graphics.GetHdc(), 20, 20, width, height,
                            srcHdc, 0, 0, width, height, TernaryRasterOperations.SRCCOPY);

                        e.Graphics.ReleaseHdc();

                        //select original object
                        IntPtr pNew = SelectObject(srcHdc, pOrig);
                        //delete objects
                        DeleteObject(pNew);
                        DeleteDC(srcHdc);
                        */


            /* Rectangle rect = new Rectangle(100,
                             0,
                            test2.Width - 100,
                             test2.Height

                 );*/

            //e.Graphics.DrawImageUnscaledAndClipped(test.ToBitmap(),rect);

            IntPtr dcMem;
            SolidBrush m_Brush = new SolidBrush(Color.FromArgb(0, 0, 0));

            dcMem = CreateCompatibleDC(e.Graphics.GetHdc());
            e.Graphics.ReleaseHdc();
            IntPtr bmMem;
            //bmMem = stretchDIBbits.CreateCompatibleBitmap(hDCRef.GetHdc(), GetSelectedBitmapWidth(), GetSelectedBitmapHeight());
            bmMem = stretchDIBbits.CreateCompatibleBitmap(e.Graphics.GetHdc(), 200, 200);
            e.Graphics.ReleaseHdc();
            stretchDIBbits.SelectObject(dcMem, bmMem);
            byte[] pbyteBits = new byte[200 * 200 * 3];
            int numRead = stretchDIBbits.GetDIBits(e.Graphics.GetHdc(), bmMem, 0, 200,
                pbyteBits, ref bmi, stretchDIBbits.DIB_Color_Mode.DIB_RGB_COLORS);
            e.Graphics.ReleaseHdc();
            //hDCRef.ReleaseHdc();

            Bitmap bmpScreenshot = Screenshot();
            pictureBox1.Image = bmpScreenshot;

            e.Graphics.DrawImage(test,100,100,100,100);
            using (Graphics g = Graphics.FromImage(test))
            {
                g.DrawLine(Pens.Red, 0, 0, 200, 200);
            }
            

        }
        private Bitmap Screenshot()
        {

            // This is where we will store a snapshot of the screen
            /*  Bitmap bmpScreenshot =
                  new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
  */
            Bitmap bmpScreenshot = new Bitmap(200, 200);
            // Creates a graphic object so we can draw the screen in the bitmap (bmpScreenshot);
            

            using (var g = Graphics.FromImage(bmpScreenshot))
            {
                g.CopyFromScreen(0, 0, 0, 0, new Size(200,200));
                return bmpScreenshot;
            }
        }
        public static byte[] converterDemo(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }
        public Bitmap ConvertToBitmap(string fileName)
        {
            Bitmap bitmap;
            using (Stream bmpStream = System.IO.File.Open(fileName, System.IO.FileMode.Open))
            {
                Image image = Image.FromStream(bmpStream);

                bitmap = new Bitmap(image);

            }
            return bitmap;
        }


    }
}
