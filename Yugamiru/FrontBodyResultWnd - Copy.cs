﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Yugamiru.stretchDIBbits;
using System.Drawing.Drawing2D;

namespace Yugamiru
{
    public class FrontBodyResultWnd
    {

        public byte[] m_pbyteBits;
        stretchDIBbits.BITMAPINFO m_bmi;
        
        byte[] m_pbyteBitsBackground;

        
        public int m_iOffscreenWidth;
        public int m_iOffscreenHeight;
        int m_iBackgroundWidth;
        int m_iBackgroundHeight;
        int m_iSrcX;
        int m_iSrcY;
        int m_iSrcWidth;
        int m_iSrcHeight;

        int m_iMouseCaptureMode;
        int m_iMousePointXOnDragStart;
        int m_iMousePointYOnDragStart;
        int m_iScreenWidthOnDragStart;
        int m_iScreenHeightOnDragStart;
        int m_iSrcXOnDragStart;
        int m_iSrcYOnDragStart;
        int m_iSrcWidthOnDragStart;
        int m_iSrcHeightOnDragStart;

        int m_iDataVersion;
        int m_iMarkerSize;
        int m_iMarker;
        int m_iArrowLength;
        int m_iArrowWidth;
        int m_iArrowInvisible;
        int m_iLabelInvisible;

        bool m_bMidLine;
        uint m_uiMidLineWidth;
        Color m_crMidLineColor;
        uint m_uiMidLineStyle;

        bool m_bStyleLine;
        uint m_uiStyleLineStyle;
        Color m_crStyleLineColor;
        uint m_uiStyleLineWidth;

        Color m_crFontColor;
        bool m_bOutline;
        Color m_crOutlineColor;

        FrontBodyPosition m_FrontBodyPosition;
        FrontBodyAngle m_FrontBodyAngle;
        FrontBodyResultData m_FrontBodyResultData;
        
        Point m_ptBenchmark1;
        Point m_ptBenchmark2;
        int m_iBenchmarkDistance;
        Font m_lf;
        public FrontBodyResultWnd()
        {

             m_pbyteBits = null;



            m_iOffscreenWidth = 0;

    m_iOffscreenHeight = 0;

            m_iBackgroundWidth = 0;

            m_iBackgroundHeight = 0;

            m_iSrcX = 0;

            m_iSrcY = 0;

            m_iSrcWidth = 0;

            m_iSrcHeight = 0;

            m_iMouseCaptureMode = 0;

            m_iMousePointXOnDragStart = 0;

            m_iMousePointYOnDragStart = 0;

            m_iScreenWidthOnDragStart = 0;

            m_iScreenHeightOnDragStart = 0;

            m_iSrcXOnDragStart = 0;

            m_iSrcYOnDragStart = 0;

            m_iSrcWidthOnDragStart = 0;

            m_iSrcHeightOnDragStart = 0;

            m_iDataVersion = 0;

            m_iMarkerSize = 0;

            m_iArrowLength = 0;

            m_iArrowWidth = 0;

            m_iArrowInvisible = 0;

            m_iLabelInvisible = 0;

            m_bMidLine = false; 

    m_uiMidLineWidth = 0;

            m_crMidLineColor = Color.FromArgb(0, 0, 0);

    m_uiMidLineStyle = 0;

            m_bStyleLine = false;

    m_uiStyleLineStyle = 0;

            m_crStyleLineColor = Color.FromArgb(0, 0, 0);

    m_uiStyleLineWidth = 0;

            m_crFontColor = Color.FromArgb(0, 0, 0);

            m_bOutline = false;

            m_crOutlineColor = Color.FromArgb(0, 0, 0);

            //m_FrontBodyPosition = new FrontBodyPosition();

            m_FrontBodyAngle = new FrontBodyAngle();
            

            m_FrontBodyResultData = new FrontBodyResultData();

            m_ptBenchmark1 = new Point(0, 0);

            m_ptBenchmark2 = new Point(0, 0);

            m_iBenchmarkDistance = 0;
                      }

  public bool SetBackgroundBitmap(int iWidth, int iHeight, byte[] pbyteBits )
{

    m_bmi.bmiHeader.biSize          = 40;//sizeof(BITMAPINFOHEADER);
	m_bmi.bmiHeader.biWidth			= iWidth;
	m_bmi.bmiHeader.biHeight		= -iHeight;
	m_bmi.bmiHeader.biPlanes		= 1;
	m_bmi.bmiHeader.biBitCount		= 24;
	m_bmi.bmiHeader.biCompression	= 0;
	m_bmi.bmiHeader.biSizeImage		= 0;
	m_bmi.bmiHeader.biXPelsPerMeter	= 0;
	m_bmi.bmiHeader.biYPelsPerMeter	= 0;
	m_bmi.bmiHeader.biClrUsed		= 0;
	m_bmi.bmiHeader.biClrImportant	= 0;

            m_bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            m_bmi.bmiColors[0].rgbBlue		= 255;
	m_bmi.bmiColors[0].rgbGreen		= 255;
	m_bmi.bmiColors[0].rgbRed		= 255;
	m_bmi.bmiColors[0].rgbReserved	= 255;

	m_iBackgroundWidth	= iWidth;
	m_iBackgroundHeight	= iHeight;

	int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
        int iBmpBitsSize = iBmpWidthStep * iHeight;
	if ( iBmpBitsSize <= 0 ){
		return false;
	}
	
m_pbyteBits = new byte[iBmpBitsSize];
	if ( m_pbyteBits == null ){
		return false;
	}
	int i = 0;
	for( i = 0; i<iBmpBitsSize; i++ ){
		m_pbyteBits[i] = pbyteBits[i];
	}
	m_iSrcX = 0;
	m_iSrcY = 0;
	m_iSrcWidth = m_iBackgroundWidth;
	m_iSrcHeight = m_iBackgroundHeight;

	
	return true;
}
  public void UpdateOffscreen(Graphics m_pDCOffscreen)
        {
       
            int iBmpWidthStep = (m_iBackgroundWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * m_iBackgroundHeight;
            //int i = 0;
            /*for (i = 0; i < iBmpBitsSize; i++)
            {
                m_pbyteBitsBackground[i] = m_pbyteBits[i];
            }*/

            stretchDIBbits.SetStretchBltMode(m_pDCOffscreen.GetHdc(), stretchDIBbits.StretchBltMode.STRETCH_HALFTONE);
            m_pDCOffscreen.ReleaseHdc();
            stretchDIBbits.StretchDIBits(m_pDCOffscreen.GetHdc(), 0, 0, m_iOffscreenWidth, m_iOffscreenHeight,
                       m_iSrcX, m_iSrcY, m_iSrcWidth, m_iSrcHeight, m_pbyteBits, ref m_bmi,
                            Constants.DIB_RGB_COLORS,
                            Constants.SRCCOPY);
            m_pDCOffscreen.ReleaseHdc();
            

            m_iArrowLength = (int)(50 * m_iBackgroundWidth / (m_iSrcWidth * Constants.IMG_SCALE_MAX));
            m_iArrowWidth = (int)(8 * m_iBackgroundWidth / (m_iSrcWidth * Constants.IMG_SCALE_MAX));


            GlyphOverlayerToFrontImage GlyphOverlayer = new GlyphOverlayerToFrontImage();
            GlyphOverlayer.SetArrowLength(m_iArrowLength);
            GlyphOverlayer.SetArrowWidth(m_iArrowWidth);
            GlyphOverlayer.SetArrowInvisible(m_iArrowInvisible);

            Point ptRightAnkle = new Point(0,0);
            Point ptLeftAnkle = new Point(0,0);
            m_FrontBodyPosition.GetRightAnklePosition(ref ptRightAnkle);
            m_FrontBodyPosition.GetLeftAnklePosition(ref ptLeftAnkle);
            double iAnkleCenterXPos =(double)( (ptRightAnkle.X + ptLeftAnkle.X) / 2)*
            ((double)m_iOffscreenWidth/(double)m_iBackgroundWidth);

            //double iAnkleCenterXPos = (ptRightAnkle.X + ptLeftAnkle.X) / 2;


            GlyphOverlayer.SetCenterLineData(
                m_bMidLine,
                (int)m_uiMidLineStyle,
                m_crMidLineColor,
                (int)m_uiMidLineWidth,
                (int)iAnkleCenterXPos,
                0,
                m_iOffscreenHeight);//m_iBackgroundHeight);

            GlyphOverlayer.SetJointConnectionLineData(
                m_bStyleLine,
                (int)m_uiStyleLineStyle,
                m_crStyleLineColor,
                (int)m_uiStyleLineWidth);

            GlyphOverlayer.ptRightShoulder.X = (m_FrontBodyPosition.m_ptRightShoulder.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptRightShoulder.Y = (m_FrontBodyPosition.m_ptRightShoulder.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptRightAnkle.X = (m_FrontBodyPosition.m_ptRightAnkle.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptRightAnkle.Y = (m_FrontBodyPosition.m_ptRightAnkle.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptRightHip.X = (m_FrontBodyPosition.m_ptRightHip.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptRightHip.Y = (m_FrontBodyPosition.m_ptRightHip.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptRightKnee.X = (m_FrontBodyPosition.m_ptRightKnee.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptRightKnee.Y = (m_FrontBodyPosition.m_ptRightKnee.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

            GlyphOverlayer.ptLeftShoulder.X = (m_FrontBodyPosition.m_ptLeftShoulder.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptLeftShoulder.Y = (m_FrontBodyPosition.m_ptLeftShoulder.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptLeftAnkle.X = (m_FrontBodyPosition.m_ptLeftAnkle.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptLeftAnkle.Y = (m_FrontBodyPosition.m_ptLeftAnkle.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptLeftHip.X = (m_FrontBodyPosition.m_ptLeftHip.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptLeftHip.Y = (m_FrontBodyPosition.m_ptLeftHip.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptLeftKnee.X = (m_FrontBodyPosition.m_ptLeftKnee.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptLeftKnee.Y = (m_FrontBodyPosition.m_ptLeftKnee.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;


            GlyphOverlayer.ptGlabella.X = (m_FrontBodyPosition.m_ptGlabella.X - m_iSrcX)* m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptGlabella.Y = (m_FrontBodyPosition.m_ptGlabella.Y - m_iSrcY) *m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptRightEar.X = (m_FrontBodyPosition.m_ptRightEar.X - m_iSrcX)* m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptRightEar.Y = (m_FrontBodyPosition.m_ptRightEar.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptLeftEar.X = (m_FrontBodyPosition.m_ptLeftEar.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptLeftEar.Y = (m_FrontBodyPosition.m_ptLeftEar.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            GlyphOverlayer.ptChin.X = (m_FrontBodyPosition.m_ptChin.X - m_iSrcX)* m_iOffscreenWidth / m_iSrcWidth;
            GlyphOverlayer.ptChin.Y = (m_FrontBodyPosition.m_ptChin.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

            m_iMarker = (int)(m_iMarkerSize * m_iBackgroundWidth / (m_iSrcWidth * Constants.IMG_SCALE_MAX));


            GlyphOverlayer.SetMarkerSize(m_iMarker);
            GlyphOverlayer.SetFrontBodyPosition(m_FrontBodyPosition);
            GlyphOverlayer.SetFrontBodyResultData(m_FrontBodyResultData);
            GlyphOverlayer.Draw(m_pDCOffscreen); 

            

            //LOGFONT lf = m_lf;
            FontFamily fontFamily = new FontFamily("Arial");
            Font lf = new Font(
               fontFamily,
               (m_iOffscreenHeight * 35) / 1000,//16,
            FontStyle.Regular,
               GraphicsUnit.Pixel);
            //lf.Height = -(m_iOffscreenHeight * 35) / 1000;
            //CFont fontNew;
            //fontNew.CreateFontIndirect(&lf);

            FrontBodyLabelString temp_FrontBodyLabelString = new FrontBodyLabelString(m_FrontBodyPosition, m_FrontBodyAngle,
                m_ptBenchmark1, m_ptBenchmark2, m_iBenchmarkDistance);

            //Rectangle rcDraw = new Rectangle(0, 0, m_iOffscreenWidth, m_iOffscreenHeight );
            if (m_iLabelInvisible > 0)
            {                
                LabelandFont(m_pDCOffscreen, temp_FrontBodyLabelString);
            }
        }

        public void LabelandFont(Graphics e, FrontBodyLabelString m_FrontBodyLabelString)
        {
            Font f = new Font("calibri light",  12, FontStyle.Regular, GraphicsUnit.Pixel);

            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.GLABELLA_POS, f, new Point(14, 6));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strGlabellaPosition, f, new Point(14, 32));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_POS, f, new Point(14, 70));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strEarCenterPosition, f, new Point(14, 96));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.CHIN_POS, f, new Point(14, 141));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strChinPosition, f, new Point(14, 167));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_POS, f, new Point(14, 212));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strShoulderCenterPosition, f, new Point(14, 238));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_POS, f, new Point(14, 283));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strHipCenterPosition, f, new Point(14, 309));

            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_ANGLE, f, new Point(227, 6));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strEarBalance, f, new Point(241, 32));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_ANGLE, f, new Point(192, 70));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strShoulderBalance, f, new Point(241, 96));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_ANGLE, f, new Point(227, 141));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strHipBalance, f, new Point(248, 167));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.RIGHT_KNEE_ANGLE, f, new Point(178, 212));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strRightKneeAngle, f, new Point(248, 238));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.LEFT_KNEE_ANGLE, f, new Point(185, 283));
            SampleDrawOutlineText(e, m_FrontBodyLabelString.m_strLeftKneeAngle, f, new Point(241, 309));

           
        }
        void SampleDrawOutlineText(Graphics g, String text, Font font, Point p)
        {
            // set atialiasing
            g.SmoothingMode = SmoothingMode.HighQuality;
            // make thick pen for outlining
            Pen pen = new Pen(Color.White, 3);
            // round line joins of the pen
            pen.LineJoin = LineJoin.Round;
            // create graphics path
            GraphicsPath textPath = new GraphicsPath();
            // convert string to path
            textPath.AddString(text, font.FontFamily, (int)font.Style, font.Size, p, StringFormat.GenericTypographic);
            // clone path to make outlining path
            GraphicsPath outlinePath = (GraphicsPath)textPath.Clone();
            // outline the path
            outlinePath.Widen(pen);
            // fill outline path with some color
            g.FillPath(Brushes.White, outlinePath);
            // fill original text path with some color
            g.FillPath(Brushes.Black, textPath);
        }

        public void SetSrcPos(int iXPos, int iYPos)
        {
            m_iSrcX = iXPos;
            m_iSrcY = iYPos;
        }

        public void SetSrcSize(int iWidth, int iHeight)
        {
            m_iSrcWidth = iWidth;
            m_iSrcHeight = iHeight;
        }

        public void OnRButtonDown(MouseEventArgs point)
        {
            //CWnd::OnRButtonDown(nFlags, point);

            if (m_iMouseCaptureMode != 0)
            {
                return;
            }

            //SetCapture();
            m_iMouseCaptureMode = 2;

            Rectangle rcClient = new Rectangle();
            //GetClientRect(&rcClient);

            m_iMousePointXOnDragStart = point.X;
            m_iMousePointYOnDragStart = point.Y;
            m_iScreenWidthOnDragStart = 304;//rcClient.Width;
            m_iScreenHeightOnDragStart = 380;// rcClient.Height;
            m_iSrcXOnDragStart = m_iSrcX;
            m_iSrcYOnDragStart = m_iSrcY;
            m_iSrcWidthOnDragStart = m_iSrcWidth;
            m_iSrcHeightOnDragStart = m_iSrcHeight;
           /* if (GetParent() != NULL)
            {
                GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
            }*/
        }

        public void OnRButtonUp(MouseEventArgs point)
        {
            //CWnd::OnRButtonUp(nFlags, point);
            if (m_iMouseCaptureMode != 2)
            {
                return;
            }
            //ReleaseCapture();
            m_iMouseCaptureMode = 0;
       /*     if (GetParent() != NULL)
            {
                GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
            }*/
        }

        public void OnMouseMove(MouseEventArgs point)
        {
            //CWnd::OnMouseMove(nFlags, point);

            if (m_iMouseCaptureMode == 2)
            {
                int iDiffX = point.X - m_iMousePointXOnDragStart;
                int iDiffY = point.Y - m_iMousePointYOnDragStart;
                m_iSrcX = m_iSrcXOnDragStart - iDiffX * m_iSrcWidthOnDragStart / m_iScreenWidthOnDragStart;
                m_iSrcY = m_iSrcYOnDragStart - iDiffY * m_iSrcHeightOnDragStart / m_iScreenHeightOnDragStart;
                {
                    char[] szMsg = new char[256];
                    Console.Write("RatioX is %f RatioY is %f\r\n",
                        (double)m_iSrcWidthOnDragStart / m_iScreenWidthOnDragStart,
                        (double)m_iSrcHeightOnDragStart / m_iScreenHeightOnDragStart);
                    //			OutputDebugString( szMsg );
                }
                if (m_iSrcX < 0)
                {
                    m_iSrcX = 0;
                }
                if (m_iSrcY < 0)
                {
                    m_iSrcY = 0;
                }
                if (m_iSrcX + m_iSrcWidthOnDragStart >= m_iBackgroundWidth)
                {
                    m_iSrcX = m_iBackgroundWidth - m_iSrcWidthOnDragStart;
                }
                if (m_iSrcY + m_iSrcHeightOnDragStart >= m_iBackgroundHeight)
                {
                    m_iSrcY = m_iBackgroundHeight - m_iSrcHeightOnDragStart;
                }
           /*     if (GetParent() != NULL)
                {
                    GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                }
                UpdateOffscreen();
                CRect rcClient;
                GetClientRect(&rcClient);
                InvalidateRect(&rcClient);
                UpdateWindow();*/
            }
        }

        public int GetSrcX() 
       {
	return m_iSrcX;
}

    public int GetSrcY() 
{
	return m_iSrcY;
}

public int GetSrcWidth() 
{
	return m_iSrcWidth;
}

public int GetSrcHeight() 
{
	return m_iSrcHeight;
}

public int GetBackgroundWidth() 
{
	return m_iBackgroundWidth;
}

public int GetBackgroundHeight() 
{
	return m_iBackgroundHeight;
}

public int GetMouseCaptureMode() 
{
	return m_iMouseCaptureMode;
}

        public void SetDataVersion(int iDataVersion)
{
    m_iDataVersion = iDataVersion;
}

        public void SetMarkerSize(int iMarkerSize)
{
    m_iMarkerSize = iMarkerSize;
}

        public void SetArrowLength(int iArrowLength)
{
    m_iArrowLength = iArrowLength;
}

        public void SetArrowWidth(int iArrowWidth)
{
    m_iArrowWidth = iArrowWidth;
}

        public void SetArrowInvisible(int iArrowInvisible)
{
    m_iArrowInvisible = iArrowInvisible;
}

        public void SetLabelInvisible(int iLabelInvisible)
{
    m_iLabelInvisible = iLabelInvisible;
}

        public void SetStyleLine(bool bStyleLine)
{
    m_bStyleLine = bStyleLine;
}

        public void SetStyleLineStyle(uint uiStyleLineStyle)
{
    m_uiStyleLineStyle = uiStyleLineStyle;
}

        public void SetStyleLineColor(Color crStyleLineColor)
{
    m_crStyleLineColor = crStyleLineColor;
}

        public void SetStyleLineWidth(uint uiStyleLineWidth)
{
    m_uiStyleLineWidth = uiStyleLineWidth;
}

        public void SetMidLine(bool bMidLine)
{
    m_bMidLine = bMidLine;
}

        public void SetMidLineWidth(uint uiMidLineWidth)
{
    m_uiMidLineWidth = uiMidLineWidth;
}

        public void SetMidLineColor(Color crMidLineColor)
{
    m_crMidLineColor = crMidLineColor;
}

        public void SetMidLineStyle(uint uiMidLineStyle)
{
    m_uiMidLineStyle = uiMidLineStyle;
}

        public void SetFontColor(Color crFontColor)
{
    m_crFontColor = crFontColor;
}

        public void SetOutline(bool bOutline)
{
    m_bOutline = bOutline;
}

        public void SetOutlineColor(Color crOutlineColor)
{
    m_crOutlineColor = crOutlineColor;
}

public void SetFrontBodyPosition( FrontBodyPosition temp_FrontBodyPosition )
{
    m_FrontBodyPosition = temp_FrontBodyPosition;
}

public void SetFrontBodyAngle( FrontBodyAngle temp_FrontBodyAngle )
{
    m_FrontBodyAngle = temp_FrontBodyAngle;
}

public void SetFrontBodyResultData( FrontBodyResultData temp_FrontBodyResultData )
{
    m_FrontBodyResultData = temp_FrontBodyResultData;
}

public void SetLogFont( Font lf )
{
    m_lf = lf;
}

public void SetBenchmark1Point( Point ptBenchmark1 )
{
    m_ptBenchmark1 = ptBenchmark1;
}

public void SetBenchmark2Point( Point ptBenchmark2 )
{
    m_ptBenchmark2 = ptBenchmark2;
}

public void SetBenchmarkDistance(int iBenchmarkDistance)
{
    m_iBenchmarkDistance = iBenchmarkDistance;
}

    }
}
