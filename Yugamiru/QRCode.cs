﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    class QRCode
    {

        /*# include "stdafx.h"
        # include "QRCode.h"*/

        // ‰Â•Ï’·ƒf[ƒ^ŒêƒNƒ‰ƒX.
        class CVariableLengthDataWord
        {
            /*  public:
      CVariableLengthDataWord(void );
              CVariableLengthDataWord( const CVariableLengthDataWord &rSrc );
      ~CVariableLengthDataWord( void );
      CVariableLengthDataWord &operator=( const CVariableLengthDataWord &rSrc );
      int GetValue(void ) const;
              int GetBits(void ) const;
              void SetData(int iValue, int iBits);*/

            int m_iValue;
            int m_iBits;
            public CVariableLengthDataWord()
            {

                m_iValue = 0;
                m_iBits = 0;

            }

            CVariableLengthDataWord(CVariableLengthDataWord rSrc)
            {

                m_iValue = rSrc.m_iValue;

                m_iBits = rSrc.m_iBits;

            }
            /*
            CVariableLengthDataWord::~CVariableLengthDataWord( void )
            {
            }

            CVariableLengthDataWord &CVariableLengthDataWord::operator=( const CVariableLengthDataWord &rSrc )
            {
                m_iValue = rSrc.m_iValue;
                m_iBits = rSrc.m_iBits;
                return *this;
            }*/

            public int GetValue()
            {
                return m_iValue;
            }

            public int GetBits()
            {
                return m_iBits;
            }

            public void SetData(int iValue, int iBits)
            {
                m_iValue = iValue;
                m_iBits = iBits;
            }

        };

        // ‰Â•Ï’·ƒf[ƒ^Œê”z—ñƒNƒ‰ƒX.
        public class CVariableLengthDataWordArray
        {
            /* public:
     CVariableLengthDataWordArray(void );
             CVariableLengthDataWordArray( const CVariableLengthDataWordArray &rSrc );
     ~CVariableLengthDataWordArray( void );
     CVariableLengthDataWordArray &operator=( const CVariableLengthDataWordArray &rSrc );
     int GetSize(void ) const;
             int GetRegisterCount(void ) const;
             int GetVersion(void ) const;
             int GetEccType(void ) const;
             int GetValue(int iIndex) const;
             int GetBits(int iIndex) const;
             int CountTotalBits(void ) const;
             int SetData(int iEncodeType, int iVersion, int iEccType, const char* pchSrc, int iSize );
             int CalcMinimumVersion(void ) const;
             private:
     // Žw’è‚³‚ê‚½ƒf[ƒ^‚ðŠi”[‚Å‚«‚éÅ¬ƒo[ƒWƒ‡ƒ“‚ð‹‚ß‚é.
     static int CalcMinimumVersionByBodyDataSize(int iEncodeType, int iEccType, int iDataSizeInBits);
             int AllocateMemory(int iNewSize);
             int AddData(int iValue, int iBits);
             static int NumericModeEncode(char chSrc1, char chSrc2, char chSrc3);
             static int AlphaNumericModeEncode(char chSrc1, char chSrc2);
             static int KanjiModeEncode(char chSrc1, char chSrc2);
             int SetNumericModeData( const char* pchSrc, int iSize );
             int SetAlphaNumericModeData( const char* pchSrc, int iSize );
             int SetBinaryModeData( const char* pchSrc, int iSize );
             int SetKanjiModeData( const char* pchSrc, int iSize );*/

            int m_iSize;
            int m_iRegisterCount;
            int m_iVersion;
            int m_iEccType;
            CVariableLengthDataWord[] m_pVariableLengthDataWord;

            public CVariableLengthDataWordArray()
            {

                m_iSize = 0;

                m_iRegisterCount = 0;

                m_iVersion = 0;

                m_iEccType = -1;

                m_pVariableLengthDataWord = null;

            }

            CVariableLengthDataWordArray(CVariableLengthDataWordArray rSrc)
            {

                m_iSize = 0;

                m_iRegisterCount = 0;

                m_iVersion = rSrc.m_iVersion;

                m_iEccType = rSrc.m_iEccType;

                m_pVariableLengthDataWord = null;

                if (rSrc.m_iSize > 0)
                {
                    m_pVariableLengthDataWord = new CVariableLengthDataWord[rSrc.m_iSize];
                    if (m_pVariableLengthDataWord != null)
                    {
                        m_iSize = rSrc.m_iSize;
                        m_iRegisterCount = rSrc.m_iRegisterCount;
                        int i = 0;
                        for (i = 0; i < m_iSize; i++)
                        {
                            m_pVariableLengthDataWord[i] = rSrc.m_pVariableLengthDataWord[i];
                        }
                    }
                }
            }

            ~CVariableLengthDataWordArray()
            {
                if (m_pVariableLengthDataWord != null)
                {
                    //delete[] m_pVariableLengthDataWord;
                    m_pVariableLengthDataWord = null;
                }
                m_iSize = 0;
                m_iRegisterCount = 0;
                m_iVersion = 0;
                m_iEccType = -1;
            }

            /*  CVariableLengthDataWordArray ( CVariableLengthDataWordArray rSrc )
               {
                   if (m_pVariableLengthDataWord != NULL)
                   {
                       delete[] m_pVariableLengthDataWord;
                       m_pVariableLengthDataWord = NULL;
                   }
                   m_iSize = 0;
                   m_iRegisterCount = 0;
                   m_iVersion = rSrc.m_iVersion;
                   m_iEccType = rSrc.m_iEccType;
                   if (rSrc.m_iSize > 0)
                   {
                       m_pVariableLengthDataWord = new CVariableLengthDataWord[rSrc.m_iSize];
                       if (m_pVariableLengthDataWord != NULL)
                       {
                           m_iSize = rSrc.m_iSize;
                           m_iRegisterCount = rSrc.m_iRegisterCount;
                           int i = 0;
                           for (i = 0; i < m_iSize; i++)
                           {
                               m_pVariableLengthDataWord[i] = rSrc.m_pVariableLengthDataWord[i];
                           }
                       }
                   }
                   return *this;
               }*/

            public int GetSize()
            {
                return m_iSize;
            }

            public int GetRegisterCount()
            {
                return m_iRegisterCount;
            }

            public int GetVersion()
            {
                return m_iVersion;
            }

            public int GetEccType()
            {
                return m_iEccType;
            }

            public int GetValue(int iIndex)
            {
                if (iIndex < 0)
                {
                    return 0;
                }
                if (iIndex >= m_iSize)
                {
                    return 0;
                }
                if (iIndex >= m_iRegisterCount)
                {
                    return 0;
                }
                return m_pVariableLengthDataWord[iIndex].GetValue();
            }

            public int GetBits(int iIndex)
            {
                if (iIndex < 0)
                {
                    return 0;
                }
                if (iIndex >= m_iSize)
                {
                    return 0;
                }
                if (iIndex >= m_iRegisterCount)
                {
                    return 0;
                }
                return m_pVariableLengthDataWord[iIndex].GetBits();
            }

            public int CountTotalBits()
            {
                int iRet = 0;
                int i = 0;
                for (i = 0; i < m_iRegisterCount; i++)
                {
                    iRet += m_pVariableLengthDataWord[i].GetBits();
                }
                return iRet;
            }

            public int CalcMinimumVersion()
            {
                if (m_iEccType < 0)
                {
                    return -1;
                }
                if (m_iEccType > 3)
                {
                    return -1;
                }
                int iDataSizeInBits = CountTotalBits();
                int iVersion = 1;
                for (iVersion = 1; iVersion <= 40; iVersion++)
                {
                    if (CalcMaxDataSizeInBit(iVersion, m_iEccType) >= iDataSizeInBits)
                    {
                        return iVersion;
                    }
                }
                return -1;
            }

            public int AllocateMemory(int iNewSize)
            {
                if (m_pVariableLengthDataWord != null)
                {
                    // delete[] m_pVariableLengthDataWord;
                    m_pVariableLengthDataWord = null;
                }
                m_iSize = 0;
                m_iRegisterCount = 0;
                if (iNewSize > 0)
                {
                    m_pVariableLengthDataWord = new CVariableLengthDataWord[iNewSize];
                    // Initialize CVariableLengthDataWord object inside the array
                    for (int i = 0; i < m_pVariableLengthDataWord.Length; i++)
                    {
                        m_pVariableLengthDataWord[i] = new CVariableLengthDataWord();
                    }

                    if (m_pVariableLengthDataWord == null)
                    {
                        return 0;
                    }
                    m_iSize = iNewSize;
                }
                return 1;
            }

            int CalcMinimumVersionByBodyDataSize(int iEncodeType, int iEccType, int iDataSizeInBits)
            {
                int iVersion = 1;
                for (iVersion = 1; iVersion <= 40; iVersion++)
                {
                    if (CalcMaxDataSizeExceptHeaderPartInBit(iEncodeType, iVersion, iEccType) >= iDataSizeInBits)
                    {
                        return iVersion;
                    }
                }
                return -1;
            }

            int AddData(int iValue, int iBits)
            {
                if (m_iRegisterCount >= m_iSize)
                {
                    return 0;
                }
                m_pVariableLengthDataWord[m_iRegisterCount++].SetData(iValue, iBits);
                return 1;
            }

            int NumericModeEncode(byte chSrc1, byte chSrc2, byte chSrc3)
            {
                if (chSrc1 == '\0')
                {
                    return -1;
                }
                int iRet = 0;
                int i = 0;
                for (i = 0; i < 3; i++)
                {
                    byte chSrc = 0;
                    switch (i)
                    {
                        case 0: chSrc = chSrc1; break;
                        case 1: chSrc = chSrc2; break;
                        case 2: chSrc = chSrc3; break;
                    }
                    if (chSrc == '\0')
                    {
                        break;
                    }
                    if (chSrc < '0')
                    {
                        return -1;
                    }
                    if (chSrc > '9')
                    {
                        return -1;
                    }
                    iRet = iRet * 10 + (chSrc - '0');
                }
                return iRet;
            }

            int AlphaNumericModeEncode(byte chSrc1, byte chSrc2)
            {
                if (chSrc1 == '\0')
                {
                    return -1;
                }
                int iRet = 0;
                int i = 0;
                for (i = 0; i < 2; i++)
                {
                    byte chSrc = 0;
                    switch (i)
                    {
                        case 0: chSrc = chSrc1; break;
                        case 1: chSrc = chSrc2; break;
                    }
                    int iValue = 0;
                    if (chSrc == '\0')
                    {
                        break;
                    }
                    else if (('0' <= chSrc) && (chSrc <= '9'))
                    {
                        iValue = (chSrc - '0');
                    }
                    else if (('A' <= chSrc) && (chSrc <= 'Z'))
                    {
                        iValue = (chSrc - 'A' + 10);
                    }
                    else if (('a' <= chSrc) && (chSrc <= 'z'))
                    {
                        iValue = (chSrc - 'a' + 10);
                    }
                    else
                    {
                        switch (chSrc)
                        {
                            case (byte)' ': iValue = 36; break;
                            case (byte)'$': iValue = 37; break;
                            case (byte)'%': iValue = 38; break;
                            case (byte)'*': iValue = 39; break;
                            case (byte)'+': iValue = 40; break;
                            case (byte)'-': iValue = 41; break;
                            case (byte)'.': iValue = 42; break;
                            case (byte)'/': iValue = 43; break;
                            case (byte)':': iValue = 44; break;
                            default: return -1;
                        }
                    }
                    iRet = iRet * 45 + iValue;
                }
                return iRet;
            }

            int KanjiModeEncode(byte chSrc1, byte chSrc2)
            {
                int iTmp = ((byte)chSrc1 << 8) | ((byte)chSrc2);
                if ((0x8140 <= iTmp) && (iTmp <= 0x9FFC))
                {
                    iTmp -= 0x8140;
                    return ((((iTmp >> 8) & 0xFF) * 0xC0) + (iTmp & 0xFF));
                }
                else if ((0xE040 <= iTmp) && (iTmp <= 0xEBBF))
                {
                    iTmp -= 0xC140;
                }
                else
                {
                    return -1;
                }
                return ((((iTmp >> 8) & 0xFF) * 0xC0) + (iTmp & 0xFF));
            }

            public int SetNumericModeData(byte[] pchSrc, int iSize)
            {
                // ƒf[ƒ^ƒ`ƒFƒbƒN.
                int i = 0;
                for (i = 0; i < iSize / 3; i++)
                {
                    if (NumericModeEncode(pchSrc[i * 3 + 0], pchSrc[i * 3 + 1], pchSrc[i * 3 + 2]) < 0)
                    {
                        return 0;
                    }
                }
                if (iSize % 3 == 2)
                {
                    if (NumericModeEncode(pchSrc[i * 3 + 0], pchSrc[i * 3 + 1], (byte)'\0') < 0)
                    {
                        return 0;
                    }
                }
                else if (iSize % 3 == 1)
                {
                    if (NumericModeEncode(pchSrc[i * 3 + 0], (byte)'\0', (byte)'\0') < 0)
                    {
                        return 0;
                    }
                }
                // Å¬ƒo[ƒWƒ‡ƒ“ŒvŽZ.
                int iDataSizeInBits = (iSize / 3) * 10;
                switch (iSize % 3)
                {
                    case 1: iDataSizeInBits += 4; break;
                    case 2: iDataSizeInBits += 7; break;
                    default: break;
                }
                {
                    int iVersion = CalcMinimumVersionByBodyDataSize(Constants.ENCODEMODE_NUMERIC, m_iEccType, iDataSizeInBits);
                    if (iVersion < 0)
                    {
                        return 0;
                    }
                    if ((1 <= m_iVersion) && (m_iVersion <= 40))
                    {
                        if (m_iVersion < iVersion)
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        m_iVersion = iVersion;
                    }
                }
                // ƒƒ‚ƒŠŠm•Û.
                if (AllocateMemory(iSize + 32) <= 0)
                {
                    return 0;
                }
                AddData(1 << Constants.ENCODEMODE_NUMERIC, 4);// ƒ‚[ƒhŽwŽ¦Žq.
                AddData(iSize, CalcBitLengthOfStringLengthSpecifier(Constants.ENCODEMODE_NUMERIC, m_iVersion)); // •¶Žš’·ŽwŽ¦Žq.
                for (i = 0; i < iSize / 3; i++)
                {
                    AddData(NumericModeEncode(pchSrc[i * 3 + 0], pchSrc[i * 3 + 1], pchSrc[i * 3 + 2]), 10);
                }
                if (iSize % 3 == 2)
                {
                    AddData(NumericModeEncode(pchSrc[i * 3 + 0], pchSrc[i * 3 + 1], (byte)'\0'), 7);
                }
                else if (iSize % 3 == 1)
                {
                    AddData(NumericModeEncode(pchSrc[i * 3 + 0], (byte)'\0', (byte)'\0'), 4);
                }
                // I’[ƒpƒ^[ƒ“‚Ì’Ç‰Á.
                int iTerminatorSizeInBits = CalcMaxDataSizeExceptHeaderPartInBit(Constants.ENCODEMODE_NUMERIC, m_iVersion, m_iEccType) - iDataSizeInBits;
                if (iTerminatorSizeInBits > 0)
                {
                    iTerminatorSizeInBits = (iTerminatorSizeInBits > 4) ? 4 : iTerminatorSizeInBits;
                    AddData(0, iTerminatorSizeInBits);
                }
                return 1;
            }

            public int SetAlphaNumericModeData(byte[] pchSrc, int iSize)
            {
                // ƒf[ƒ^ƒ`ƒFƒbƒN.
                int i = 0;
                for (i = 0; i < iSize / 2; i++)
                {
                    if (AlphaNumericModeEncode(pchSrc[i * 2 + 0], pchSrc[i * 2 + 1]) < 0)
                    {
                        return 0;
                    }
                }
                if (iSize % 2 == 1)
                {
                    if (AlphaNumericModeEncode(pchSrc[i * 2 + 0], (byte)'\0') < 0)
                    {
                        return 0;
                    }
                }
                // Å¬ƒo[ƒWƒ‡ƒ“ŒvŽZ.
                int iDataSizeInBits = (iSize / 2) * 11;
                if (iSize % 2 != 0)
                {
                    iDataSizeInBits += 6;
                }
                {
                    int iVersion = CalcMinimumVersionByBodyDataSize(Constants.ENCODEMODE_ALPHANUMERIC, m_iEccType, iDataSizeInBits);
                    if (iVersion < 0)
                    {
                        return 0;
                    }
                    if ((1 <= m_iVersion) && (m_iVersion <= 40))
                    {
                        if (m_iVersion < iVersion)
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        m_iVersion = iVersion;
                    }
                }
                // ƒƒ‚ƒŠŠm•Û.
                if (AllocateMemory(iSize + 32) <= 0)
                {
                    return 0;
                }
                AddData(1 << Constants.ENCODEMODE_ALPHANUMERIC, 4);// ƒ‚[ƒhŽwŽ¦Žq.
                AddData(iSize, CalcBitLengthOfStringLengthSpecifier(Constants.ENCODEMODE_ALPHANUMERIC, m_iVersion)); // •¶Žš’·ŽwŽ¦Žq.	
                for (i = 0; i < iSize / 2; i++)
                {
                    AddData(AlphaNumericModeEncode(pchSrc[i * 2 + 0], pchSrc[i * 2 + 1]), 11);
                }
                if (iSize % 2 == 1)
                {
                    AddData(AlphaNumericModeEncode(pchSrc[i * 2 + 0], (byte)'\0'), 6);
                }
                // I’[ƒpƒ^[ƒ“‚Ì’Ç‰Á.
                int iTerminatorSizeInBits = CalcMaxDataSizeExceptHeaderPartInBit(Constants.ENCODEMODE_ALPHANUMERIC, m_iVersion, m_iEccType) - iDataSizeInBits;
                if (iTerminatorSizeInBits > 0)
                {
                    iTerminatorSizeInBits = (iTerminatorSizeInBits > 4) ? 4 : iTerminatorSizeInBits;
                    AddData(0, iTerminatorSizeInBits);
                }
                return 1;
            }

            public int SetBinaryModeData(byte[] pchSrc, int iSize)
            {
                // Å¬ƒo[ƒWƒ‡ƒ“ŒvŽZ.
                int iDataSizeInBits = iSize * 8;
                {
                    int iVersion = CalcMinimumVersionByBodyDataSize(Constants.ENCODEMODE_BINARY, m_iEccType, iDataSizeInBits);
                    if (iVersion < 0)
                    {
                        return 0;
                    }
                    if ((1 <= m_iVersion) && (m_iVersion <= 40))
                    {
                        if (m_iVersion < iVersion)
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        m_iVersion = iVersion;
                    }
                }
                // ƒƒ‚ƒŠŠm•Û.
                if (AllocateMemory(iSize + 32) <= 0)
                {
                    return 0;
                }
                AddData(1 << Constants.ENCODEMODE_BINARY, 4);// ƒ‚[ƒhŽwŽ¦Žq.
                AddData(iSize, CalcBitLengthOfStringLengthSpecifier(Constants.ENCODEMODE_BINARY, m_iVersion)); // •¶Žš’·ŽwŽ¦Žq.	
                int i = 0;
                for (i = 0; i < iSize; i++)
                {
                    AddData(pchSrc[i], 8);
                }
                // I’[ƒpƒ^[ƒ“‚Ì’Ç‰Á.
                int iTerminatorSizeInBits = CalcMaxDataSizeExceptHeaderPartInBit(Constants.ENCODEMODE_BINARY, m_iVersion, m_iEccType) - iDataSizeInBits;
                if (iTerminatorSizeInBits > 0)
                {
                    iTerminatorSizeInBits = (iTerminatorSizeInBits > 4) ? 4 : iTerminatorSizeInBits;
                    AddData(0, iTerminatorSizeInBits);
                }
                return 1;
            }

            int SetKanjiModeData(byte[] pchSrc, int iSize)
            {
                // ƒf[ƒ^ƒ`ƒFƒbƒN.
                int i = 0;
                for (i = 0; i < iSize / 2; i++)
                {
                    if (KanjiModeEncode(pchSrc[i * 2 + 0], pchSrc[i * 2 + 1]) < 0)
                    {
                        return 0;
                    }
                }
                // Å¬ƒo[ƒWƒ‡ƒ“ŒvŽZ.
                int iDataSizeInBits = (iSize / 2) * 13;
                {
                    int iVersion = CalcMinimumVersionByBodyDataSize(Constants.ENCODEMODE_KANJI, m_iEccType, iDataSizeInBits);
                    if (iVersion < 0)
                    {
                        return 0;
                    }
                    if ((1 <= m_iVersion) && (m_iVersion <= 40))
                    {
                        if (m_iVersion < iVersion)
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        m_iVersion = iVersion;
                    }
                }
                // ƒƒ‚ƒŠŠm•Û.
                if (AllocateMemory(iSize + 32) <= 0)
                {
                    return 0;
                }
                AddData(1 << Constants.ENCODEMODE_KANJI, 4);// ƒ‚[ƒhŽwŽ¦Žq.
                AddData(iSize, CalcBitLengthOfStringLengthSpecifier(Constants.ENCODEMODE_KANJI, m_iVersion)); // •¶Žš’·ŽwŽ¦Žq.	
                for (i = 0; i < iSize / 2; i++)
                {
                    AddData(KanjiModeEncode(pchSrc[i * 2 + 0], pchSrc[i * 2 + 1]), 13);
                }
                // I’[ƒpƒ^[ƒ“‚Ì’Ç‰Á.
                int iTerminatorSizeInBits = CalcMaxDataSizeExceptHeaderPartInBit(Constants.ENCODEMODE_KANJI, m_iVersion, m_iEccType) - iDataSizeInBits;
                if (iTerminatorSizeInBits > 0)
                {
                    iTerminatorSizeInBits = (iTerminatorSizeInBits > 4) ? 4 : iTerminatorSizeInBits;
                    AddData(0, iTerminatorSizeInBits);
                }
                return 1;
            }

            public int SetData(int iEncodeType, int iVersion, int iEccType, byte[] pchSrc, int iSize)
            {
                if (iEccType < 0)
                {
                    return 0;
                }
                if (iEccType > 3)
                {
                    return 0;
                }
                if (iVersion < 0)
                {
                    return 0;
                }
                if (iVersion > 40)
                {
                    return 0;
                }
                m_iVersion = iVersion;
                m_iEccType = iEccType;
                switch (iEncodeType)
                {
                    case Constants.ENCODEMODE_NUMERIC:
                        return SetNumericModeData(pchSrc, iSize);
                    case Constants.ENCODEMODE_ALPHANUMERIC:
                        return SetAlphaNumericModeData(pchSrc, iSize);
                    case Constants.ENCODEMODE_BINARY:
                        return SetBinaryModeData(pchSrc, iSize);
                    case Constants.ENCODEMODE_KANJI:
                        return SetKanjiModeData(pchSrc, iSize);
                    default:
                        break;
                }
                return 0;
            }



        };

        // ŒÅ’è’·ƒf[ƒ^Œê”z—ñƒNƒ‰ƒX.
        public class CFixedLengthDataWordArray
        {
            /* public:
     CFixedLengthDataWordArray(void );
             CFixedLengthDataWordArray( const CFixedLengthDataWordArray &rSrc );
     ~CFixedLengthDataWordArray( void );
     CFixedLengthDataWordArray &operator=( const CFixedLengthDataWordArray &rSrc );
     int GetSize(void ) const;
             int GetVersion(void ) const;
             int GetEccType(void ) const;
             int GetCode(int iIndex) const;
             int Merge( const CVariableLengthDataWordArray &VariableLengthDataWordArray );
 private:
     int AllocateMemory(int iNewSize);
             int AddData(int iValue, int iBits);
             void FillRemnantArea(void );*/
            //   private:
            int m_iSize;
            int m_iBytePosition;
            int m_iBitPosition;
            int m_iVersion;
            int m_iEccType;
            //unsigned char* m_pbyteCode;
            byte[] m_pbyteCode;

            //UIntPtr m_pbyteCode;

            public CFixedLengthDataWordArray()
            {

                m_iSize = 0;

                m_iBytePosition = 0;

                m_iBitPosition = 0;

                m_iVersion = 0;

                m_iEccType = -1;

                m_pbyteCode = null;

            }

            CFixedLengthDataWordArray(CFixedLengthDataWordArray rSrc)
            {

                m_iSize = 0;

                m_iBytePosition = 0;

                m_iBitPosition = 0;

                m_iVersion = rSrc.m_iVersion;

                m_iEccType = rSrc.m_iEccType;

                m_pbyteCode = null;

                if (rSrc.m_iSize > 0)
                {
                    m_pbyteCode = new byte[rSrc.m_iSize];
                    if (m_pbyteCode != null)
                    {
                        m_iSize = rSrc.m_iSize;
                        m_iBytePosition = rSrc.m_iBytePosition;
                        m_iBitPosition = rSrc.m_iBitPosition;
                        int i = 0;
                        for (i = 0; i < m_iSize; i++)
                        {
                            m_pbyteCode[i] = rSrc.m_pbyteCode[i];
                        }
                    }
                }
            }

            ~CFixedLengthDataWordArray()
            {
                if (m_pbyteCode != null)
                {
                    //delete[] m_pbyteCode;
                    m_pbyteCode = null;
                }
                m_iSize = 0;
                m_iBytePosition = 0;
                m_iBitPosition = 0;
                m_iVersion = 0;
                m_iEccType = -1;
            }

            /*  CFixedLengthDataWordArray &CFixedLengthDataWordArray::operator=( const CFixedLengthDataWordArray &rSrc )
              {
                  if (m_pbyteCode != NULL)
                  {
                      delete[] m_pbyteCode;
                      m_pbyteCode = NULL;
                  }
                  m_iSize = 0;
                  m_iBytePosition = 0;
                  m_iBitPosition = 0;
                  m_iVersion = rSrc.m_iVersion;
                  m_iEccType = rSrc.m_iEccType;
                  if (rSrc.m_iSize > 0)
                  {
                      m_pbyteCode = new unsigned char[rSrc.m_iSize];
                      if (m_pbyteCode != NULL)
                      {
                          m_iSize = rSrc.m_iSize;
                          m_iBytePosition = rSrc.m_iBytePosition;
                          m_iBitPosition = rSrc.m_iBitPosition;
                          int i = 0;
                          for (i = 0; i < m_iSize; i++)
                          {
                              m_pbyteCode[i] = rSrc.m_pbyteCode[i];
                          }
                      }
                  }
                  return *this;
              }*/

            public int GetSize()
            {
                return m_iSize;
            }

            public int AllocateMemory(int iNewSize)
            {
                if (m_pbyteCode != null)
                {
                    //delete[] m_pbyteCode;
                    m_pbyteCode = null;
                }
                m_iSize = 0;
                if (iNewSize > 0)
                {
                    m_pbyteCode = new byte[iNewSize];
                    if (m_pbyteCode == null)
                    {
                        return 0;
                    }
                    m_iSize = iNewSize;
                    int i = 0;
                    for (i = 0; i < iNewSize; i++)
                    {
                        m_pbyteCode[i] = 0;
                    }
                }
                return 1;
            }

            public int GetVersion()
            {
                return m_iVersion;
            }

            public int GetEccType()
            {
                return m_iEccType;
            }

            public byte GetCode(int iIndex)
            {
                if (iIndex < 0)
                {
                    return 0;
                }
                if (iIndex >= m_iSize)
                {
                    return 0;
                }
                return m_pbyteCode[iIndex];
            }

            public int AddData(int iValue, int iBits)
            {
                if (m_iBytePosition * 8 + m_iBitPosition + iBits > m_iSize * 8)
                {
                    return 0;
                }
                while (m_iBitPosition + iBits >= 8)
                {
                    int iExtractBits = 8 - m_iBitPosition;
                    int iShiftCount = iBits - iExtractBits;
                    m_pbyteCode[m_iBytePosition++] |= (byte)((iValue >> iShiftCount) & (~(0xFFFFFFFF << iExtractBits)));
                    m_iBitPosition = 0;
                    iBits -= iExtractBits;
                }
                if (m_iBitPosition + iBits > 0)
                {
                    int iShiftCount = 8 - (m_iBitPosition + iBits);
                    m_pbyteCode[m_iBytePosition] |= (byte)(iValue << iShiftCount);
                    m_iBitPosition += iBits;
                    iBits = 0;
                }
                return 1;
            }

            public void FillRemnantArea()
            {
                if (m_iBitPosition > 0)
                {
                    m_iBytePosition++;
                    m_iBitPosition = 0;
                }
                int i = 0;
                while (m_iBytePosition < m_iSize)
                {
                    m_pbyteCode[m_iBytePosition++] = (byte)(((i & 1) == 0) ? 0xEC : 0x11);
                    i++;
                }
            }

            public int Merge(CVariableLengthDataWordArray VariableLengthDataWordArray)
            {
                VariableLengthDataWordArray = new CVariableLengthDataWordArray();
                m_iSize = 0;
                m_iBytePosition = 0;
                m_iBitPosition = 0;
                m_iVersion = VariableLengthDataWordArray.GetVersion();
                m_iEccType = VariableLengthDataWordArray.GetEccType();
                int iMaxDataSizeInByte = CalcMaxDataSizeInByte(m_iVersion, m_iEccType);
                if (AllocateMemory(iMaxDataSizeInByte) <= 0)
                {
                    return 0;
                }
                int i = 0;
                for (i = 0; i < VariableLengthDataWordArray.GetRegisterCount(); i++)
                {
                    int iValue = VariableLengthDataWordArray.GetValue(i);
                    int iBits = VariableLengthDataWordArray.GetBits(i);
                    AddData(iValue, iBits);
                }
                FillRemnantArea();
                return 1;
            }


        };

        // ƒKƒƒA‘Ì(256)•„†‚ÌæŽZƒe[ƒuƒ‹
        class CGaloisField256CodeMultiplyer
        {
            /*    public:
        CGaloisField256CodeMultiplyer(void );
                CGaloisField256CodeMultiplyer( const CGaloisField256CodeMultiplyer &rSrc );
        ~CGaloisField256CodeMultiplyer( void );
        CGaloisField256CodeMultiplyer &operator=( const CGaloisField256CodeMultiplyer &rSrc );
        int Calc(int iParam1, int iParam2) const;
                int CalcByExp(int iParam1, int iParamExp) const;
                int CalcLog(int iParam) const;*/
            //private:
            byte[] m_auchExponent2Bits = new byte[256];
            byte[] m_auchBits2Exponent = new byte[256];


            public CGaloisField256CodeMultiplyer()
            {
                int iTemp = 1;
                //x8+x4+x3+x2+1
                int i = 0;
                for (i = 0; i < 256; i++)
                {
                    m_auchExponent2Bits[i] = (byte)iTemp;
                    iTemp = iTemp << 1;
                    if ((iTemp & 0x100) <= 0)
                    {
                        iTemp ^= 0x11D; // 0x11D = Œ´Žn‘½€Ž®.
                    }
                }
                m_auchBits2Exponent[0] = 0;
                for (i = 0; i < 255; i++)
                {
                    m_auchBits2Exponent[m_auchExponent2Bits[i]] = (byte)i;
                }
            }

            CGaloisField256CodeMultiplyer(CGaloisField256CodeMultiplyer rSrc)
            {
                int i = 0;
                for (i = 0; i < 256; i++)
                {
                    m_auchExponent2Bits[i] = rSrc.m_auchExponent2Bits[i];
                    m_auchBits2Exponent[i] = rSrc.m_auchBits2Exponent[i];
                }
            }

            ~CGaloisField256CodeMultiplyer()
            {
            }
            /*
            CGaloisField256CodeMultiplyer &CGaloisField256CodeMultiplyer::operator=( const CGaloisField256CodeMultiplyer &rSrc )
            {
                int i = 0;
                for (i = 0; i < 256; i++)
                {
                    m_auchExponent2Bits[i] = rSrc.m_auchExponent2Bits[i];
                    m_auchBits2Exponent[i] = rSrc.m_auchBits2Exponent[i];
                }
                return *this;
            }
            */
            public int Calc(int iParam1, int iParam2)
            {
                iParam1 &= 0xFF;
                iParam2 &= 0xFF;
                if (iParam1 == 0)
                {
                    return 0;
                }
                if (iParam2 == 0)
                {
                    return 0;
                }
                int iExp1 = m_auchBits2Exponent[iParam1];
                int iExp2 = m_auchBits2Exponent[iParam2];
                int iExp3 = (iExp1 + iExp2) % 255;
                return m_auchExponent2Bits[iExp3];
            }

            public byte CalcByExp(int iParam1, int iParamExp)
            {
                iParam1 &= 0xFF;
                if (iParam1 == 0)
                {
                    return 0;
                }
                int iExp1 = m_auchBits2Exponent[iParam1];
                int iExp3 = (iExp1 + iParamExp) % 255;
                return m_auchExponent2Bits[iExp3];
            }

            public int CalcLog(int iParam)
            {
                return (m_auchBits2Exponent[iParam & 0xFF]);
            }

        };

        // ƒŠ[ƒhƒ\ƒƒ‚ƒ“•„†‚Ì¶¬‘½€Ž®
        class CReedSolomonCodeGeneratingPolinomial
        {
            /*  public:
      CReedSolomonCodeGeneratingPolinomial(void );
              CReedSolomonCodeGeneratingPolinomial( const CReedSolomonCodeGeneratingPolinomial &rSrc );
      ~CReedSolomonCodeGeneratingPolinomial( void );
      CReedSolomonCodeGeneratingPolinomial &operator=( CReedSolomonCodeGeneratingPolinomial &rSrc );
      void PrintContent( const CGaloisField256CodeMultiplyer &CodeMultiplyer );
      int Setup( const CGaloisField256CodeMultiplyer &CodeMultiplyer, int iNewSize );
              int Encode( const CGaloisField256CodeMultiplyer &CodeMultiplyer, unsigned char* puchCode, int iCodeSize ) const;

              private:*/
            int m_iSize;
            //unsigned char* m_pbyteData;
            byte[] m_pbyteData;

            public CReedSolomonCodeGeneratingPolinomial()
            {

                m_iSize = 0;

                m_pbyteData = null;

            }

            CReedSolomonCodeGeneratingPolinomial(CReedSolomonCodeGeneratingPolinomial rSrc)
            {

                m_iSize = 0;

                m_pbyteData = null;

                if (rSrc.m_iSize > 0)
                {
                    m_pbyteData = new byte[rSrc.m_iSize];
                    if (m_pbyteData != null)
                    {
                        m_iSize = rSrc.m_iSize;
                        int i = 0;
                        for (i = 0; i < m_iSize; i++)
                        {
                            m_pbyteData[i] = rSrc.m_pbyteData[i];
                        }
                    }
                }
            }

            ~CReedSolomonCodeGeneratingPolinomial()
            {
                if (m_pbyteData != null)
                {
                    //delete[] m_pbyteData;
                    m_pbyteData = null;
                }
                m_iSize = 0;
            }
            /*
                CReedSolomonCodeGeneratingPolinomial &CReedSolomonCodeGeneratingPolinomial::operator=(CReedSolomonCodeGeneratingPolinomial &rSrc )
                {
                    if (m_pbyteData != NULL)
                    {
                        delete[] m_pbyteData;
                        m_pbyteData = NULL;
                    }
                    m_iSize = 0;
                    if (rSrc.m_iSize > 0)
                    {
                        m_pbyteData = new unsigned char[rSrc.m_iSize];
                        if (m_pbyteData != NULL)
                        {
                            m_iSize = rSrc.m_iSize;
                            int i = 0;
                            for (i = 0; i < m_iSize; i++)
                            {
                                m_pbyteData[i] = rSrc.m_pbyteData[i];
                            }
                        }
                    }
                    return *this;
                }
                */
            public int Setup(CGaloisField256CodeMultiplyer GaloisField256CodeMultiplyer, int iNewSize)
            {
                if (m_pbyteData != null)
                {
                    //delete[] m_pbyteData;
                    m_pbyteData = null;
                }
                m_iSize = 0;
                if (iNewSize > 0)
                {
                    m_pbyteData = new byte[iNewSize];
                    byte[] pbyteDataWork = new byte[iNewSize];
                    if ((m_pbyteData == null) || (pbyteDataWork == null))
                    {
                        if (m_pbyteData != null)
                        {
                            //delete[] m_pbyteData;
                            m_pbyteData = null;
                        }
                        if (pbyteDataWork != null)
                        {
                            //delete[] pbyteDataWork;
                            pbyteDataWork = null;
                        }
                        return 0;
                    }

                    m_iSize = iNewSize;
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        m_pbyteData[i] = 0;
                        pbyteDataWork[i] = 0;
                    }
                    i = 0;
                    m_pbyteData[i++] = 1;
                    int iParamExp = 0;
                    while (i < m_iSize)
                    {
                        int j = 0;
                        pbyteDataWork[0] = 0;
                        for (j = 0; j < i; j++)
                        {
                            pbyteDataWork[j + 1] = GaloisField256CodeMultiplyer.CalcByExp(m_pbyteData[j], iParamExp);
                        }
                        i++;
                        for (j = 0; j < i; j++)
                        {
                            m_pbyteData[j] = (byte)(m_pbyteData[j] ^ pbyteDataWork[j]);
                        }
                        PrintContent(GaloisField256CodeMultiplyer);
                        iParamExp++;
                    }
                    if (pbyteDataWork != null)
                    {
                        //delete[] pbyteDataWork;
                        pbyteDataWork = null;
                    }
                }
                return 1;
            }

            public int Encode(CGaloisField256CodeMultiplyer GaloisField256CodeMultiplyer, byte[] puchCode, int iCodeSize)
            {
                int iOffset = 0;
                while (iCodeSize - iOffset >= m_iSize)
                {
                    if (puchCode[iOffset] != 0)
                    {
                        byte uchFactor = puchCode[iOffset];
                        int i = 0;
                        for (i = 0; i < m_iSize; i++)
                        {
                            puchCode[iOffset + i] ^= (byte)(GaloisField256CodeMultiplyer.Calc(m_pbyteData[i], uchFactor));
                        }
                        for (i = 0; i < iCodeSize; i++)
                        {
                            if (i % 10 == 0)
                            {

                                //printf("\r\n");
                                Console.WriteLine("\r\n");
                            }

                            //printf( "%3d ", puchCode[i] );
                            Console.WriteLine("%3d", puchCode[i]);
                        }

                        //printf("\r\n");
                        Console.WriteLine("\r\n");
                    }
                    iOffset++;
                }
                return iOffset;
            }

            public void PrintContent(CGaloisField256CodeMultiplyer GaloisField256CodeMultiplyer)
            {
                //#if 0
                int i = 0;
                for (i = 0; i < m_iSize; i++)
                {
                    if (i % 10 == 0)
                    {
                        //printf("\r\n");
                        Console.WriteLine("\r\n");
                    }
                    //printf("%3d ", CodeMultiplyer.CalcLog( m_pbyteData[i] ) );
                    //Console.WriteLine("%3d", CodeMultiplyer.CalcLog(m_pbyteData[i])); -- need to check later
                }
                //printf("\r\n");
                Console.WriteLine("\r\n");
                //#endif
            }


        };

        // ’ù³•„†”z—ñ
        class CErrorCorrectionCodeArray
        {
            /*   public:
       CErrorCorrectionCodeArray(void );
               CErrorCorrectionCodeArray( const CErrorCorrectionCodeArray &rSrc );
       ~CErrorCorrectionCodeArray( void );
       CErrorCorrectionCodeArray &operator=( const CErrorCorrectionCodeArray &rSrc );
       int GetSize(void ) const;
               int GetValidSize(void ) const;
               int GetVersion(void ) const;
               int GetEccType(void ) const;
               int GetCode(int iIndex) const;
               int Setup( const CFixedLengthDataWordArray &FixedLengthDataWordArray );
   private:*/
            int m_iSize;
            int m_iValidSize;
            int m_iVersion;
            int m_iEccType;
            //unsigned char* m_pbyteData;
            byte[] m_pbyteData;

            public CErrorCorrectionCodeArray()
            {

                m_iSize = 0;

                m_iValidSize = 0;

                m_iVersion = 0;

                m_iEccType = -1;

                m_pbyteData = null;

            }

            CErrorCorrectionCodeArray(CErrorCorrectionCodeArray rSrc)
            {

                m_iSize = 0;

                m_iValidSize = 0;

                m_iVersion = rSrc.m_iVersion;

                m_iEccType = rSrc.m_iEccType;

                m_pbyteData = null;

                if (rSrc.m_iSize > 0)
                {
                    m_pbyteData = new byte[rSrc.m_iSize];
                    if (m_pbyteData != null)
                    {
                        m_iSize = rSrc.m_iSize;
                        m_iValidSize = rSrc.m_iValidSize;
                        int i = 0;
                        for (i = 0; i < m_iSize; i++)
                        {
                            m_pbyteData[i] = rSrc.m_pbyteData[i];
                        }
                    }
                }
            }

            ~CErrorCorrectionCodeArray()
            {
                if (m_pbyteData != null)
                {
                    //delete[] m_pbyteData;
                    m_pbyteData = null;
                }
                m_iSize = 0;
                m_iValidSize = 0;
                m_iVersion = 0;
                m_iEccType = -1;
            }
            /*
                CErrorCorrectionCodeArray &CErrorCorrectionCodeArray::operator=( const CErrorCorrectionCodeArray &rSrc )
                {
                    if (m_pbyteData != NULL)
                    {
                        delete[] m_pbyteData;
                        m_pbyteData = NULL;
                    }
                    m_iSize = 0;
                    m_iValidSize = 0;
                    m_iVersion = rSrc.m_iVersion;
                    m_iEccType = rSrc.m_iEccType;
                    if (rSrc.m_iSize > 0)
                    {
                        m_pbyteData = new unsigned char[rSrc.m_iSize];
                        if (m_pbyteData != NULL)
                        {
                            m_iSize = rSrc.m_iSize;
                            m_iValidSize = rSrc.m_iValidSize;
                            int i = 0;
                            for (i = 0; i < m_iSize; i++)
                            {
                                m_pbyteData[i] = rSrc.m_pbyteData[i];
                            }
                        }
                    }
                    return *this;
                }
                */
            public int GetSize()
            {
                return m_iSize;
            }

            public int GetValidSize()
            {
                return m_iValidSize;
            }

            public int GetVersion()
            {
                return m_iVersion;
            }

            public int GetEccType()
            {
                return m_iEccType;
            }

            public byte GetCode(int iIndex)
            {
                if (iIndex < 0)
                {
                    return 0;
                }
                if (iIndex >= m_iValidSize)
                {
                    return 0;
                }
                return m_pbyteData[iIndex];
            }

            public int Setup(CFixedLengthDataWordArray FixedLengthDataWordArray)
            {
                m_iVersion = FixedLengthDataWordArray.GetVersion();
                m_iEccType = FixedLengthDataWordArray.GetEccType();

                int iBodyBlockSize = CalcTotalBlockSizeInByte(m_iVersion, m_iEccType) - CalcErrorCorrectionCodeBlockSizeInByte(m_iVersion, m_iEccType);
                int iBlockCount1 = CalcBlockCount1(m_iVersion, m_iEccType);
                int iBlockCount2 = CalcBlockCount2(m_iVersion, m_iEccType);
                int iEccBlockSize = CalcErrorCorrectionCodeBlockSizeInByte(m_iVersion, m_iEccType);

                if (FixedLengthDataWordArray.GetSize() != (iBodyBlockSize * iBlockCount1 + (iBodyBlockSize + 1) * iBlockCount2))
                {
                    return 0;
                }
                CGaloisField256CodeMultiplyer GaloisField256CodeMultiplyer = new CGaloisField256CodeMultiplyer();
                CReedSolomonCodeGeneratingPolinomial ReedSolomonCodeGeneratingPolinomial = new CReedSolomonCodeGeneratingPolinomial();
                if ((ReedSolomonCodeGeneratingPolinomial.Setup(GaloisField256CodeMultiplyer, iEccBlockSize + 1)) <= 0)
                {
                    return 0;
                }
                ReedSolomonCodeGeneratingPolinomial.PrintContent(GaloisField256CodeMultiplyer);

                int iNewSize = /* FixedLengthDataWordArray.GetSize() +*/ iEccBlockSize * (iBlockCount1 + iBlockCount2);
                if (iBlockCount2 > 0)
                {
                    iNewSize += iBodyBlockSize + 1;
                }
                else
                {
                    iNewSize += iBodyBlockSize;
                }
                if (m_pbyteData != null)
                {
                    //delete[] m_pbyteData;
                    m_pbyteData = null;
                }
                m_iSize = 0;
                m_iValidSize = 0;
                if (iNewSize <= 0)
                {
                    return 1;
                }
                m_pbyteData = new byte[iNewSize];
                if (m_pbyteData == null)
                {
                    return 0;
                }
                m_iSize = iNewSize;
                m_iValidSize = /* FixedLengthDataWordArray.GetSize() +*/ iEccBlockSize * (iBlockCount1 + iBlockCount2);
                int i = 0;
                //while( i < FixedLengthDataWordArray.GetSize() ){
                //	m_pbyteData[i++] = FixedLengthDataWordArray.GetCode( i );
                //}
                while (i < m_iSize)
                {
                    m_pbyteData[i++] = 0;
                }
                int iSrcOffset = 0;
                int iDstOffset = 0;//FixedLengthDataWordArray.GetSize();
                for (i = 0; i < iBlockCount1; i++)
                {
                    int j = 0;
                    for (j = 0; j < iBodyBlockSize; j++)
                    {
                        m_pbyteData[iDstOffset + j] = (byte)FixedLengthDataWordArray.GetCode(iSrcOffset + j);
                    }
                    for (; j < iBodyBlockSize + iEccBlockSize; j++)
                    {
                        m_pbyteData[iDstOffset + j] = 0;
                    }
                    int iEccOffset = ReedSolomonCodeGeneratingPolinomial.Encode(GaloisField256CodeMultiplyer, /*m_pbyteData + iDstOffset*/m_pbyteData, iBodyBlockSize + iEccBlockSize);
                    if (iEccOffset != 0)
                    {
                        for (j = 0; j < iEccBlockSize; j++)
                        {
                            m_pbyteData[iDstOffset + j] = m_pbyteData[iDstOffset + iEccOffset + j];
                        }
                    }
                    iSrcOffset += iBodyBlockSize;
                    iDstOffset += iEccBlockSize;
                }
                for (i = 0; i < iBlockCount2; i++)
                {
                    int j = 0;
                    for (j = 0; j < iBodyBlockSize + 1; j++)
                    {
                        m_pbyteData[iDstOffset + j] = FixedLengthDataWordArray.GetCode(iSrcOffset + j);
                    }
                    for (; j < iBodyBlockSize + 1 + iEccBlockSize; j++)
                    {
                        m_pbyteData[iDstOffset + j] = 0;
                    }
                    int iEccOffset = ReedSolomonCodeGeneratingPolinomial.Encode(GaloisField256CodeMultiplyer, /*m_pbyteData + iDstOffset*/m_pbyteData, iBodyBlockSize + 1 + iEccBlockSize);
                    if (iEccOffset != 0)
                    {
                        for (j = 0; j < iEccBlockSize; j++)
                        {
                            m_pbyteData[iDstOffset + j] = m_pbyteData[iDstOffset + iEccOffset + j];
                        }
                    }
                    iSrcOffset += (iBodyBlockSize + 1);
                    iDstOffset += iEccBlockSize;
                }
                return 1;
            }



        };

        // ƒCƒ“ƒ^[ƒŠ[ƒu”z’uÏ•„†”z—ñ
        class CInterleavedCodeArray
        {
            /*  public:
      CInterleavedCodeArray(void );
              CInterleavedCodeArray( const CInterleavedCodeArray &rSrc );
      ~CInterleavedCodeArray( void );
      CInterleavedCodeArray &operator=( const CInterleavedCodeArray &rSrc );
      int GetSize(void ) const;
              int GetVersion(void ) const;
              int GetEccType(void ) const;
              int GetCode(int iIndex) const;
              int Setup( const CFixedLengthDataWordArray &FixedLengthDataWordArray,
              const CErrorCorrectionCodeArray &ErrorCorrectionCodeArray );
  private:
      int AllocateMemory(int iNewSize);
              private:*/
            int m_iSize;
            int m_iVersion;
            int m_iEccType;
            //unsigned char* m_pbyteCode;
            byte[] m_pbyteCode;

            public CInterleavedCodeArray()
            {
                m_iSize = 0;

                m_iVersion = 0;

                m_iEccType = -1;

                m_pbyteCode = null;

            }

            CInterleavedCodeArray(CInterleavedCodeArray rSrc)
            {

                m_iSize = 0;

                m_iVersion = rSrc.m_iVersion;

                m_iEccType = rSrc.m_iEccType;

                m_pbyteCode = null;

                if (rSrc.m_iSize > 0)
                {
                    m_pbyteCode = new byte[rSrc.m_iSize];
                    if (m_pbyteCode != null)
                    {
                        m_iSize = rSrc.m_iSize;
                        int i = 0;
                        for (i = 0; i < m_iSize; i++)
                        {
                            m_pbyteCode[i] = rSrc.m_pbyteCode[i];
                        }
                    }
                }
            }

            ~CInterleavedCodeArray()
            {
                if (m_pbyteCode != null)
                {
                    //delete[] m_pbyteCode;
                    m_pbyteCode = null;
                }
                m_iSize = 0;
                m_iVersion = 0;
                m_iEccType = -1;
            }
            /*
                CInterleavedCodeArray &CInterleavedCodeArray::operator=( const CInterleavedCodeArray &rSrc )
                {
                    if (m_pbyteCode != NULL)
                    {
                        delete[] m_pbyteCode;
                        m_pbyteCode = NULL;
                    }
                    m_iSize = 0;
                    m_iVersion = rSrc.m_iVersion;
                    m_iEccType = rSrc.m_iEccType;
                    if (rSrc.m_iSize > 0)
                    {
                        m_pbyteCode = new unsigned char[rSrc.m_iSize];
                        if (m_pbyteCode != NULL)
                        {
                            m_iSize = rSrc.m_iSize;
                            int i = 0;
                            for (i = 0; i < m_iSize; i++)
                            {
                                m_pbyteCode[i] = rSrc.m_pbyteCode[i];
                            }
                        }
                    }
                    return *this;
                }
            */
            public int GetSize()
            {
                return m_iSize;
            }

            public int GetVersion()
            {
                return m_iVersion;
            }

            public int GetEccType()
            {
                return m_iEccType;
            }

            public int GetCode(int iIndex)
            {
                if (iIndex < 0)
                {
                    return 0;
                }
                if (iIndex >= m_iSize)
                {
                    return 0;
                }
                return m_pbyteCode[iIndex];
            }

            public int AllocateMemory(int iNewSize)
            {
                if (m_pbyteCode != null)
                {
                    //delete[] m_pbyteCode;
                    m_pbyteCode = null;
                }
                m_iSize = 0;
                if (iNewSize > 0)
                {
                    m_pbyteCode = new byte[iNewSize];
                    if (m_pbyteCode == null)
                    {
                        return 0;
                    }
                    m_iSize = iNewSize;
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        m_pbyteCode[i] = 0;
                    }
                }
                return 1;
            }

            public int Setup(
                    CFixedLengthDataWordArray FixedLengthDataWordArray,
                     CErrorCorrectionCodeArray ErrorCorrectionCodeArray)
            {
                if (FixedLengthDataWordArray.GetVersion() != ErrorCorrectionCodeArray.GetVersion())
                {
                    return 0;
                }
                if (FixedLengthDataWordArray.GetEccType() != ErrorCorrectionCodeArray.GetEccType())
                {
                    return 0;
                }
                m_iVersion = FixedLengthDataWordArray.GetVersion();
                m_iEccType = FixedLengthDataWordArray.GetEccType();
                int iBodyBlockSize = CalcTotalBlockSizeInByte(m_iVersion, m_iEccType) - CalcErrorCorrectionCodeBlockSizeInByte(m_iVersion, m_iEccType);
                int iBlockCount1 = CalcBlockCount1(m_iVersion, m_iEccType);
                int iBlockCount2 = CalcBlockCount2(m_iVersion, m_iEccType);
                int iEccBlockSize = CalcErrorCorrectionCodeBlockSizeInByte(m_iVersion, m_iEccType);

                int iNewSize = FixedLengthDataWordArray.GetSize() + (ErrorCorrectionCodeArray.GetValidSize() /* - ByteCodeArray.GetSize() */ );
                if (AllocateMemory(iNewSize) <= 0)
                {
                    return 0;
                }
                int iSrcOffset = 0;
                int iDstOffset = 0;
                int i = 0;
                int j = 0;
                for (i = 0; i < iBodyBlockSize; i++)
                {
                    iSrcOffset = i;
                    iDstOffset = (iBlockCount1 + iBlockCount2) * i;
                    for (j = 0; j < iBlockCount1; j++)
                    {
                        m_pbyteCode[iDstOffset + j] = FixedLengthDataWordArray.GetCode(iSrcOffset + iBodyBlockSize * j);
                    }
                    iSrcOffset = iBodyBlockSize * iBlockCount1 + i;
                    iDstOffset = (iBlockCount1 + iBlockCount2) * i + iBlockCount1;
                    for (j = 0; j < iBlockCount2; j++)
                    {
                        m_pbyteCode[iDstOffset + j] = FixedLengthDataWordArray.GetCode(iSrcOffset + (iBodyBlockSize + 1) * j);
                    }
                }
                {
                    iSrcOffset = iBodyBlockSize * iBlockCount1 + iBodyBlockSize;
                    iDstOffset = (iBlockCount1 + iBlockCount2) * iBodyBlockSize;
                    for (j = 0; j < iBlockCount2; j++)
                    {
                        m_pbyteCode[iDstOffset + j] = FixedLengthDataWordArray.GetCode(iSrcOffset + (iBodyBlockSize + 1) * j);
                    }
                }
                iSrcOffset = 0;//ByteCodeArray.GetSize();
                iDstOffset = FixedLengthDataWordArray.GetSize();
                for (i = 0; i < iEccBlockSize; i++)
                {
                    for (j = 0; j < iBlockCount1 + iBlockCount2; j++)
                    {
                        m_pbyteCode[iDstOffset + (iBlockCount1 + iBlockCount2) * i + j] = ErrorCorrectionCodeArray.GetCode(iSrcOffset + iEccBlockSize * j + i);
                    }
                }
                return 1;
            }


        };

        // ‚p‚qƒR[ƒh‚Ìƒrƒbƒg‚QŽŸŒ³”z’uƒƒP[ƒ^
        class CQRCodeBitLocator
        {
            /*   public:
       CQRCodeBitLocator(void );
               CQRCodeBitLocator( const CQRCodeBitLocator &rSrc );
       ~CQRCodeBitLocator( void );
       CQRCodeBitLocator &operator=( const CQRCodeBitLocator &rSrc );
       int GetSize(void ) const;
               int Setup(int iVersion);
               int GetData(int iXPos, int iYPos) const;
               int CountTotalDataBit(void ) const;
               int LocateData(CQRCodeBitmap &QRCodeBitmap, const CInterleavedCodeArray &InterleavedByteArray, int iForEvalMask ) const;
               private:
       int LocateDataMain(CQRCodeBitmap &QRCodeBitmap, const CInterleavedCodeArray &InterleavedByteArray, int iForEvalMask, int i, int j, int iBaseBit ) const;
               private:*/
            int m_iSize;
            //unsigned char** m_ppbyteData;
            byte[,] m_ppbyteData;


            public CQRCodeBitLocator()
            {
                m_iSize = 0;

                m_ppbyteData = null;

            }

            CQRCodeBitLocator(CQRCodeBitLocator rSrc)
            {

                m_iSize = 0;

                m_ppbyteData = null;

                if (rSrc.m_iSize > 0)
                {
                    m_ppbyteData = new byte[rSrc.m_iSize, 0];
                    if (m_ppbyteData != null)
                    {
                        int i = 0;
                        // for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData = null;
                        }
                        int iError = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            //m_ppbyteData[i] = new byte[rSrc.m_iSize];
                            m_ppbyteData = new byte[rSrc.m_iSize, 0];
                            if (m_ppbyteData == null)
                            {
                                iError = 1;
                                break;
                            }
                        }
                        if (iError <= 0)
                        {
                            m_iSize = rSrc.m_iSize;
                            for (i = 0; i < m_iSize; i++)
                            {
                                int j = 0;
                                for (j = 0; j < m_iSize; j++)
                                {
                                    m_ppbyteData = new byte[i, j];
                                }
                            }
                        }
                        else
                        {
                            for (i = 0; i < rSrc.m_iSize; i++)
                            {
                                if (m_ppbyteData != null)
                                {
                                    //delete[] m_ppbyteData[i];
                                    m_ppbyteData = null;
                                }
                            }
                            //delete[] m_ppbyteData;
                            m_ppbyteData = null;
                        }
                    }
                }
            }

            ~CQRCodeBitLocator()
            {
                if (m_ppbyteData != null)
                {
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        if (m_ppbyteData != null)
                        {
                            //delete[] m_ppbyteData[i];
                            m_ppbyteData = null;
                        }
                    }
                    //delete[] m_ppbyteData;
                    m_ppbyteData = null;
                }
                m_iSize = 0;
            }
            /*
            CQRCodeBitLocator &CQRCodeBitLocator::operator=( const CQRCodeBitLocator &rSrc )
            {
                if (m_ppbyteData != NULL)
                {
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        if (m_ppbyteData[i] != NULL)
                        {
                            delete[] m_ppbyteData[i];
                            m_ppbyteData[i] = NULL;
                        }
                    }
                    delete[] m_ppbyteData;
                    m_ppbyteData = NULL;
                }
                m_iSize = 0;
                if (rSrc.m_iSize > 0)
                {
                    m_ppbyteData = new unsigned char*[rSrc.m_iSize];
                    if (m_ppbyteData != NULL)
                    {
                        int i = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData[i] = NULL;
                        }
                        int iError = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData[i] = new unsigned char[rSrc.m_iSize];
                            if (m_ppbyteData[i] == NULL)
                            {
                                iError = 1;
                                break;
                            }
                        }
                        if (!iError)
                        {
                            m_iSize = rSrc.m_iSize;
                            for (i = 0; i < m_iSize; i++)
                            {
                                int j = 0;
                                for (j = 0; j < m_iSize; j++)
                                {
                                    m_ppbyteData[i][j] = rSrc.m_ppbyteData[i][j];
                                }
                            }
                        }
                        else
                        {
                            for (i = 0; i < rSrc.m_iSize; i++)
                            {
                                if (m_ppbyteData[i] != NULL)
                                {
                                    delete[] m_ppbyteData[i];
                                    m_ppbyteData[i] = NULL;
                                }
                            }
                            delete[] m_ppbyteData;
                            m_ppbyteData = NULL;
                        }
                    }
                }
                return *this;
            }
            */
            public int GetSize()
            {
                return m_iSize;
            }

            public int GetData(int iXPos, int iYPos)
            {
                if (iXPos < 0)
                {
                    return 0;
                }
                if (iXPos >= m_iSize)
                {
                    return 0;
                }
                if (iYPos < 0)
                {
                    return 0;
                }
                if (iYPos >= m_iSize)
                {
                    return 0;
                }
                return m_ppbyteData[iYPos, iXPos];
            }

            public int Setup(int iVersion)
            {
                byte[,] s_aauchBlockCenterPos = new byte[41, 6] {
        {   0,   0,   0,   0,   0,   0 },	// 0
		{   0,   0,   0,   0,   0,   0 },	// 1
		{  18,   0,   0,   0,   0,   0 },	// 2
		{  22,   0,   0,   0,   0,   0 },	// 3
		{  26,   0,   0,   0,   0,   0 },	// 4
		{  30,   0,   0,   0,   0,   0 },	// 5
		{  34,   0,   0,   0,   0,   0 },	// 6
		{  22,  38,   0,   0,   0,   0 },	// 7
		{  24,  42,   0,   0,   0,   0 },	// 8
		{  26,  46,   0,   0,   0,   0 },	// 9
		{  28,  50,   0,   0,   0,   0 },	// 10
		{  30,  54,   0,   0,   0,   0 },	// 11
		{  32,  58,   0,   0,   0,   0 },	// 12
		{  34,  62,   0,   0,   0,   0 },	// 13
		{  26,  46,  66,   0,   0,   0 },	// 14
		{  26,  48,  70,   0,   0,   0 },	// 15
		{  26,  50,  74,   0,   0,   0 },	// 16
		{  30,  54,  78,   0,   0,   0 },	// 17
		{  30,  56,  82,   0,   0,   0 },	// 18
		{  30,  58,  86,   0,   0,   0 },	// 19
		{  34,  62,  90,   0,   0,   0 },	// 20
		{  28,  50,  72,  94,   0,   0 },	// 21
		{  26,  50,  74,  98,   0,   0 },	// 22
		{  30,  54,  78, 102,   0,   0 },	// 23
		{  28,  54,  80, 106,   0,   0 },	// 24
		{  32,  58,  84, 110,   0,   0 },	// 25
		{  30,  58,  86, 114,   0,   0 },	// 26
		{  34,  62,  90, 118,   0,   0 },	// 27
		{  26,  50,  74,  98, 122,   0 },	// 28
		{  30,  54,  78, 102, 126,   0 },	// 29
		{  26,  52,  78, 104, 130,   0 },	// 30
		{  30,  56,  82, 108, 134,   0 },	// 31
		{  34,  60,  86, 112, 138,   0 },	// 32
		{  30,  58,  86, 114, 142,   0 },	// 33
		{  34,  62,  90, 118, 146,   0 },	// 34
		{  30,  54,  78, 102, 126, 150 },	// 35
		{  24,  50,  76, 102, 128, 154 },	// 36
		{  28,  54,  80, 106, 132, 158 },	// 37
		{  32,  58,  84, 110, 136, 162 },	// 38
		{  26,  54,  82, 110, 138, 166 },	// 39
		{  30,  58,  86, 114, 142, 170 },	// 40
	};
                if (iVersion <= 0)
                {
                    return 0;
                }
                if (iVersion > 40)
                {
                    return 0;
                }
                int iNewSize = iVersion * 4 + 17;
                int i = 0;
                if (m_ppbyteData != null)
                {

                    for (i = 0; i < m_iSize; i++)
                    {
                        if (m_ppbyteData != null)
                        {
                            //delete[] m_ppbyteData[i];
                            m_ppbyteData = null;
                        }
                    }
                    //delete[] m_ppbyteData;
                    m_ppbyteData = null;
                }
                m_iSize = 0;
                if (iNewSize > 0)
                {
                    m_ppbyteData = new byte[iNewSize, iNewSize];
                    if (m_ppbyteData == null)
                    {
                        return 0;
                    }
                    //int i = 0;
                    for (i = 0; i < iNewSize; i++)
                    {
                        m_ppbyteData = null;
                    }
                    int iError = 0;
                    for (i = 0; i < iNewSize; i++)
                    {
                        m_ppbyteData = new byte[i, i];
                        if (m_ppbyteData == null)
                        {
                            iError = 1;
                            break;
                        }
                    }
                    int jj = 0;
                    if (iError <= 0)
                    {
                        m_iSize = iNewSize;

                        for (i = 0; i < m_iSize; i++)
                        {

                            for (jj = 0; jj < m_iSize; jj++)
                            {
                                m_ppbyteData[i, jj] = 1;
                            }
                        }
                    }
                    else
                    {
                        for (i = 0; i < iNewSize; i++)
                        {
                            if (m_ppbyteData != null)
                            {
                                //delete[] m_ppbyteData[i];
                                m_ppbyteData = null;
                            }
                        }
                        //delete[] m_ppbyteData;
                        m_ppbyteData = null;
                        return 0;
                    }
                }
                // ƒtƒŒ[ƒ€ƒf[ƒ^‚ÌÝ’è.
                m_ppbyteData[m_iSize - 1 - 7, 8] = 2;
                // ¶ã‚Ìƒ}ƒXƒN.
                //int i = 0;
                int j = 0;
                for (i = 0; i < 9; i++)
                {

                    for (j = 0; j < 9; j++)
                    {
                        if (((i == 0) || (i == 6)) && (0 <= j) && (j <= 6))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else if (((j == 0) || (j == 6)) && (0 <= i) && (i <= 6))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else if ((2 <= j) && (j <= 4) && (2 <= i) && (i <= 4))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else
                        {
                            m_ppbyteData[i, j] = 0;
                        }
                    }
                }
                // ‰Eã‚Ìƒ}ƒXƒN.
                //int j = 0;
                for (i = 0; i < 9; i++)
                {

                    for (j = m_iSize - 8; j < m_iSize; j++)
                    {
                        if (((i == 0) || (i == 6)) && (m_iSize - 7 <= j) && (j <= m_iSize - 1))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else if (((j == m_iSize - 7) || (j == m_iSize - 1)) && (0 <= i) && (i <= 6))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else if ((m_iSize - 5 <= j) && (j <= m_iSize - 3) && (2 <= i) && (i <= 4))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else
                        {
                            m_ppbyteData[i, j] = 0;
                        }
                    }
                }
                // ¶‰º‚Ìƒ}ƒXƒN.
                //int j = 0;
                for (i = m_iSize - 8; i < m_iSize; i++)
                {

                    for (j = 0; j < 9; j++)
                    {
                        if (((i == m_iSize - 7) || (i == m_iSize - 1)) && (0 <= j) && (j <= 6))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else if (((j == 0) || (j == 6)) && (m_iSize - 7 <= i) && (i <= m_iSize - 1))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else if ((2 <= j) && (j <= 4) && (m_iSize - 5 <= i) && (i <= m_iSize - 3))
                        {
                            m_ppbyteData[i, j] = 2;
                        }
                        else
                        {
                            m_ppbyteData[i, j] = 0;
                        }
                    }
                }
                // cüA‰¡ü‚Ìƒ}ƒXƒN.
                for (i = 0; i < m_iSize; i++)
                {
                    if ((6 < i) && (i < m_iSize - 1 - 6))
                    {
                        if (i % 2 == 0)
                        {
                            m_ppbyteData[6, i] = 2;
                            m_ppbyteData[i, 6] = 2;
                        }
                        else
                        {
                            m_ppbyteData[6, i] = 0;
                            m_ppbyteData[i, 6] = 0;
                        }
                    }
                }
                // ƒuƒƒbƒNƒ}ƒXƒN.
                int iBlockCount = 0;
                for (i = 0; i < 6; i++)
                {
                    if (s_aauchBlockCenterPos[iVersion, i] == 0)
                    {
                        break;
                    }
                    iBlockCount++;
                }
                //int j = 0;
                for (i = 0; i < iBlockCount + 1; i++)
                {
                    for (j = 0; j < iBlockCount + 1; j++)
                    {
                        if ((i < iBlockCount - 1) || (j < iBlockCount - 1) || ((i == iBlockCount - 1) && (j == iBlockCount - 1)))
                        {
                            int x = 6;
                            if (i < iBlockCount)
                            {
                                x = s_aauchBlockCenterPos[iVersion, j];
                            }
                            int y = 6;
                            if (i < iBlockCount)
                            {
                                y = s_aauchBlockCenterPos[iVersion, i];
                            }
                            int xx = 0;
                            int yy = 0;
                            for (yy = -2; yy <= +2; yy++)
                            {
                                for (xx = -2; xx <= +2; xx++)
                                {
                                    int xxx = (xx >= 0) ? xx : -xx;
                                    int yyy = (yy >= 0) ? yy : -yy;
                                    int zzz = (xxx > yyy) ? xxx : yyy;
                                    if (zzz % 2 == 0)
                                    {
                                        m_ppbyteData[y + yy, x + xx] = 2;
                                    }
                                    else
                                    {
                                        m_ppbyteData[y + yy, x + xx] = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                if (iVersion >= 7)
                {
                    // ƒo[ƒWƒ‡ƒ“6Bit‚ð¶¬‘½€Ž®0x01F25‚ÉŠî‚Ã‚«BCH•„†‰»
                    // Œë‚è’ù³•„†‚Í12Bit
                    int iMask = (iVersion << 12);
                    for (i = 5; i >= 0; i--)
                    {
                        if ((iMask & (0x01000 << i)) > 0)
                        {
                            iMask ^= (0x01F25 << i);
                        }
                    }
                    iMask |= (iVersion << 12);
                    for (i = 0; i < 18; i++)
                    {
                        if ((iMask & (1 << (17 - i))) > 0)
                        {
                            int x = m_iSize - 1 - 10 + ((17 - i) % 3);
                            int y = ((17 - i) / 3);
                            m_ppbyteData[y, x] = 2;
                            m_ppbyteData[x, y] = 2;
                        }
                    }
                }
                return 1;
            }

            public int LocateDataMain(CQRCodeBitmap SquareBitmap, CInterleavedCodeArray InterleavedCodeArray, int iForEvalMask, int i, int j, int iBaseBit)
            {
                if ((m_ppbyteData[i, j] & 0x01) > 0)
                {
                    // ƒf[ƒ^‚ðInterleavedByteCodeArray‚É‚à‚Æ‚Ã‚¢‚ÄÝ’è‚·‚é.
                    int iMaskData = 0;
                    if ((iBaseBit & 1) != 0)
                    {
                        iMaskData = 0xFF;
                    }
                    if ((j + i) % 2 == 0)
                    {
                        iMaskData ^= 1;
                    }
                    if (i % 2 == 0)
                    {
                        iMaskData ^= 2;
                    }
                    if (j % 3 == 0)
                    {
                        iMaskData ^= 4;
                    }
                    if ((i + j) % 3 == 0)
                    {
                        iMaskData ^= 8;
                    }
                    if (((i / 2) + (j / 3)) % 2 == 0)
                    {
                        iMaskData ^= 16;
                    }
                    if ((j * i) % 2 + (j * i) % 3 == 0)
                    {
                        iMaskData ^= 32;
                    }
                    if (((j * i) % 2 + (j * i) % 3) % 2 == 0)
                    {
                        iMaskData ^= 64;
                    }
                    if (((j * i) % 3 + (j + i) % 2) % 2 == 0)
                    {
                        iMaskData ^= 128;
                    }
                    SquareBitmap.m_ppbyteData[i, j] = (byte)iMaskData;
                    return 1;
                }
                else
                {
                    // ƒf[ƒ^‚ðInterleavedByteCodeArray‚Å‚Í‚È‚­A2ƒrƒbƒg–Ú‚ÉŠî‚Ã‚«Ý’è‚·‚é.				
                    //if (((( m_ppbyteData[i,j] & 0x02 ) == 0)||(iForEvalMask))){
                    if (((m_ppbyteData[i, j] & 0x02) == 0) || (iForEvalMask > 0))
                    {

                        SquareBitmap.m_ppbyteData[i, j] = 0x00;
                    }
                    else
                    {
                        SquareBitmap.m_ppbyteData[i, j] = 0xFF;
                    }
                    return 0;
                }
            }

            public int LocateData(CQRCodeBitmap QRCodeBitmap, CInterleavedCodeArray InterleavedCodeArray, int iForEvalMask)
            {
                if (QRCodeBitmap.GetSize() != m_iSize)
                {
                    if ((QRCodeBitmap.AllocateMemory(m_iSize)) <= 0)
                    {
                        return 0;
                    }
                }
                else
                {
                    QRCodeBitmap.FillData(0);
                }
                int dir = -1;
                int i = m_iSize - 1;
                int j1 = m_iSize - 1;
                int j2 = m_iSize - 2;
                int iSrcIndex = 0;
                int iSrcBitIndex = 0;
                while (((j1 >= 0) || (j2 >= 0)))
                {
                    for (; ((0 <= i) && (i <= m_iSize - 1)); i += dir)
                    {
                        int iBaseBit = 0;
                        if (j1 >= 0)
                        {
                            iBaseBit = 0;
                            if (iSrcIndex < InterleavedCodeArray.GetSize())
                            {
                                iBaseBit = (InterleavedCodeArray.GetCode(iSrcIndex) >> (7 - iSrcBitIndex)) & 1;
                            }
                            if (LocateDataMain(QRCodeBitmap, InterleavedCodeArray, iForEvalMask, i, j1, iBaseBit) > 0)
                            {
                                iSrcBitIndex++;
                                if (iSrcBitIndex >= 8)
                                {
                                    iSrcIndex++;
                                    iSrcBitIndex = 0;
                                }
                            }
                        }
                        if (j2 >= 0)
                        {
                            iBaseBit = 0;
                            if (iSrcIndex < InterleavedCodeArray.GetSize())
                            {
                                iBaseBit = (InterleavedCodeArray.GetCode(iSrcIndex) >> (7 - iSrcBitIndex)) & 1;
                            }
                            if (LocateDataMain(QRCodeBitmap, InterleavedCodeArray, iForEvalMask, i, j2, iBaseBit) > 0)
                            {
                                iSrcBitIndex++;
                                if (iSrcBitIndex >= 8)
                                {
                                    iSrcIndex++;
                                    iSrcBitIndex = 0;
                                }
                            }
                        }
                    }
                    j1 = j2 - 1;
                    if (j1 == 6)
                    {
                        j1--;
                    }
                    j2 = j1 - 1;
                    if (j2 == 6)
                    {
                        j2--;
                    }
                    dir *= -1;
                    if (dir == 1)
                    {
                        i = 0;
                    }
                    else
                    {
                        i = m_iSize - 1;
                    }
                }
                // x=6‚ÍAInterleavedDataArray‚©‚ç‚Í“Ç‚Ýž‚Ü‚È‚¢‚Ì‚ÅÅŒã‚Éˆ—‚·‚é.
                for (i = 0; i < m_iSize; i++)
                {

                    LocateDataMain(QRCodeBitmap, InterleavedCodeArray, iForEvalMask, i, 6, 0);
                }
                int[,] s_aaiFormationPos1 = new int[15, 2]{
        { 0, 8 }, { 1, 8 }, { 2, 8 }, { 3, 8 }, { 4, 8 }, { 5, 8 }, { 7, 8 }, { 8, 8 },
        { 8, 7 }, { 8, 5 }, { 8, 4 }, { 8, 3 }, { 8, 2 }, { 8, 1 }, { 8, 0 },
    };
                int[,] s_aaiFormationPos2 = new int[15, 2]{
        { 8, -1 }, { 8, -2 }, { 8, -3 }, { 8, -4 }, { 8, -5 }, { 8, -6 }, { 8, -7 },
        { -8,  8 },  { -7,  8 }, { -6, 8 }, { -5, 8 }, { -4, 8 }, { -3, 8 }, { -2, 8 }, { -1, 8 },
    };
                // ECCTYPE2ƒrƒbƒg‚Æƒ}ƒXƒNƒ^ƒCƒv3Bit‚ð¶¬‘½€Ž®0x537‚ÉŠî‚Ã‚­BCH•„†‰»iŒë‚è’ù³10Bit).
                //unsigned short aushFormationValue[8];
                byte[] aushFormationValue = new byte[8];

                int iEccType = InterleavedCodeArray.GetEccType();
                for (i = 0; i < 8; i++)
                {
                    //unsigned short ushFormationValue = ((iEccType << 3) | i) << 10;
                    byte ushFormationValue = (byte)(((iEccType << 3) | i) << 10);
                    //unsigned short ushECC = ushFormationValue;
                    byte ushECC = ushFormationValue;
                    int j = 0;
                    for (j = 4; j >= 0; j--)
                    {
                        if ((ushECC & (0x400 << j)) > 0)
                        {
                            ushECC ^= (byte)(0x537 << j);
                        }
                    }
                    // 0x5412‚ÍŽd—l‘‚É’è‚ß‚ç‚ê‚Ä‚¢‚éƒ}ƒXƒNƒpƒ^[ƒ“.
                    aushFormationValue[i] = (byte)((ushFormationValue | ushECC) ^ 0x5412);
                }
                for (i = 0; i < 15; i++)
                {
                    int x1 = s_aaiFormationPos1[i, 0];
                    int y1 = s_aaiFormationPos1[i, 1];
                    int x2 = s_aaiFormationPos2[i, 0];
                    int y2 = s_aaiFormationPos2[i, 1];
                    if (x1 < 0) { x1 += m_iSize; }
                    if (y1 < 0) { y1 += m_iSize; }
                    if (x2 < 0) { x2 += m_iSize; }
                    if (y2 < 0) { y2 += m_iSize; }
                    int val = 0;
                    int j = 0;
                    int iMaskBit = (1 << (14 - i));
                    for (j = 0; j < 8; j++)
                    {
                        if ((aushFormationValue[j] & iMaskBit) > 0)
                        {
                            val |= (1 << j);
                        }
                    }
                    QRCodeBitmap.m_ppbyteData[y1, x1] = (byte)val;
                    QRCodeBitmap.m_ppbyteData[y2, x2] = (byte)val;
                }
                return 1;
            }

            public int CountTotalDataBit()
            {
                int iRet = 0;
                int i = 0;
                int j = 0;
                for (i = 0; i < m_iSize; i++)
                {
                    for (j = 0; j < m_iSize; j++)
                    {
                        if ((m_ppbyteData[i, j] & 0x01) > 0)
                        {
                            iRet++;
                        }
                    }
                }
                return iRet;
            }



        };

        public class CQRCodeBitmap
        {
            public int m_iSize;
            public byte[,] m_ppbyteData;

            public CQRCodeBitmap()
            {

                m_iSize = 0;

                m_ppbyteData = null;

            }
            CQRCodeBitmap(CQRCodeBitmap rSrc)
            {

                m_iSize = 0;

                m_ppbyteData = null;

                if (rSrc.m_iSize > 0)
                {
                    m_ppbyteData = new byte[rSrc.m_iSize, rSrc.m_iSize];
                    if (m_ppbyteData != null)
                    {
                        int i = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData = null;
                        }
                        int iError = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData = new byte[i, i];
                            if (m_ppbyteData == null)
                            {
                                iError = 1;
                                break;
                            }
                        }
                        if (iError <= 0)
                        {
                            m_iSize = rSrc.m_iSize;
                            for (i = 0; i < m_iSize; i++)
                            {
                                int j = 0;
                                for (j = 0; j < m_iSize; j++)
                                {
                                    m_ppbyteData[i, j] = rSrc.m_ppbyteData[i, j];
                                }
                            }
                        }
                        else
                        {
                            for (i = 0; i < rSrc.m_iSize; i++)
                            {
                                if (m_ppbyteData != null)
                                {
                                    //delete[] m_ppbyteData[i];
                                    m_ppbyteData = null;
                                }
                            }
                            //delete[] m_ppbyteData;
                            m_ppbyteData = null;
                        }
                    }
                }
            }

            ~CQRCodeBitmap()
            {
                if (m_ppbyteData != null)
                {
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        if (m_ppbyteData[i, i] != null)
                        {
                            //delete[] m_ppbyteData[i];
                            m_ppbyteData = null;
                        }
                    }
                    //delete[] m_ppbyteData;
                    m_ppbyteData = null;
                }
                m_iSize = 0;
            }
            /*
            CQRCodeBitmap &CQRCodeBitmap::operator=( const CQRCodeBitmap &rSrc )
            {
                if (m_ppbyteData != NULL)
                {
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        if (m_ppbyteData[i] != NULL)
                        {
                            delete[] m_ppbyteData[i];
                            m_ppbyteData[i] = NULL;
                        }
                    }
                    delete[] m_ppbyteData;
                    m_ppbyteData = NULL;
                }
                m_iSize = 0;
                if (rSrc.m_iSize > 0)
                {
                    m_ppbyteData = new unsigned char*[rSrc.m_iSize];
                    if (m_ppbyteData != NULL)
                    {
                        int i = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData[i] = NULL;
                        }
                        int iError = 0;
                        for (i = 0; i < rSrc.m_iSize; i++)
                        {
                            m_ppbyteData[i] = new unsigned char[rSrc.m_iSize];
                            if (m_ppbyteData[i] == NULL)
                            {
                                iError = 1;
                                break;
                            }
                        }
                        if (!iError)
                        {
                            m_iSize = rSrc.m_iSize;
                            for (i = 0; i < m_iSize; i++)
                            {
                                int j = 0;
                                for (j = 0; j < m_iSize; j++)
                                {
                                    m_ppbyteData[i][j] = rSrc.m_ppbyteData[i][j];
                                }
                            }
                        }
                        else
                        {
                            for (i = 0; i < rSrc.m_iSize; i++)
                            {
                                if (m_ppbyteData[i] != NULL)
                                {
                                    delete[] m_ppbyteData[i];
                                    m_ppbyteData[i] = NULL;
                                }
                            }
                            delete[] m_ppbyteData;
                            m_ppbyteData = NULL;
                        }
                    }
                }
                return *this;
            }
            */
            public int GetSize()
            {
                return m_iSize;
            }

            public int AllocateMemory(int iNewSize)
            {
                if (m_ppbyteData != null)
                {
                    int i = 0;
                    for (i = 0; i < m_iSize; i++)
                    {
                        if (m_ppbyteData != null)
                        {
                            //delete[] m_ppbyteData[i];
                            m_ppbyteData = null;
                        }
                    }
                    //delete[] m_ppbyteData;
                    m_ppbyteData = null;
                }
                m_iSize = 0;
                if (iNewSize > 0)
                {
                    m_ppbyteData = new byte[iNewSize, iNewSize];
                    if (m_ppbyteData == null)
                    {
                        return 0;
                    }
                    int i = 0;
                    //for (i = 0; i < iNewSize; i++)
                    {
                        m_ppbyteData = null;
                    }
                    int iError = 0;
                    for (i = 0; i < iNewSize; i++)
                    {
                        m_ppbyteData = new byte[iNewSize, iNewSize];
                        if (m_ppbyteData == null)
                        {
                            iError = 1;
                            break;
                        }
                    }
                    if (iError >= 0)
                    {
                        m_iSize = iNewSize;
                        for (i = 0; i < m_iSize; i++)
                        {
                            int j = 0;
                            for (j = 0; j < m_iSize; j++)
                            {
                                m_ppbyteData[i, j] = 0;
                            }
                        }
                    }
                    else
                    {
                        //for (i = 0; i < iNewSize; i++)
                        {
                            //if (m_ppbyteData[i] != NULL)
                            {
                                //delete[] m_ppbyteData[i];
                                m_ppbyteData = null;
                            }
                        }
                        //delete[] m_ppbyteData;
                        m_ppbyteData = null;
                        return 0;
                    }
                }
                return 1;
            }
            
            public byte Index( int iIndex,int jIndex)
            {
                if (iIndex < 0)
                {
                    return new byte();
                }
                if (iIndex >= m_iSize)
                {
                    return new byte();
                }
                return m_ppbyteData[iIndex, jIndex];
            }

          /*  const unsigned char* CQRCodeBitmap::operator[]( int iIndex) const
            {
                if ( iIndex< 0 ){
                    return NULL;
                }
                if ( iIndex >= m_iSize ){
                    return NULL;
                }
                return m_ppbyteData[iIndex];
            }*/

            public void FillData(int iData)
            {
                int i = 0;
                for (i = 0; i < m_iSize; i++)
                {
                    int j = 0;
                    for (j = 0; j < m_iSize; j++)
                    {
                        m_ppbyteData[i, j] = (byte)iData;
                    }
                }
            }

            public int CalcBestMask(int iTotalBitCount)
            {
                if (iTotalBitCount <= 0)
                {
                    iTotalBitCount = 1;
                }
                int[] aiDemerit = new int[8];
                int i = 0;
                int j = 0;
                for (i = 0; i < 8; i++)
                {
                    aiDemerit[i] = 0;
                }
                int x = 0;
                int y = 0;
                // 1“_–Ú‚ÌŒ¸“_ƒ`ƒFƒbƒN€–Ú
                // X•ûŒü‚Ì5ƒrƒbƒgˆÈã‚Ì“¯’l˜A‘±.
                for (y = 0; y < m_iSize; y++)
                {
                    int[] aiLastBitValue = new int[8];
                    int[] aiLength = new int[8];
                    for (i = 0; i < 8; i++)
                    {
                        aiLastBitValue[i] = -1;
                        aiLength[i] = 0;
                    }
                    for (x = 0; x < m_iSize; x++)
                    {
                        for (i = 0; i < 8; i++)
                        {
                            if (aiLastBitValue[i] == -1)
                            {
                                aiLastBitValue[i] = ((m_ppbyteData[y, x] >> 1) & 1);
                                aiLength[i] = 1;
                            }
                            else if (aiLastBitValue[i] == ((m_ppbyteData[y, x] >> 1) & 1))
                            {
                                aiLength[i]++;
                            }
                            else
                            {
                                if (aiLength[i] >= 5)
                                {
                                    aiDemerit[i] += (aiLength[i] - 2);
                                }
                                aiLastBitValue[i] = ((m_ppbyteData[y, x] >> 1) & 1);
                                aiLength[i] = 1;
                            }
                        }
                    }
                    for (i = 0; i < 8; i++)
                    {
                        if (aiLength[i] >= 5)
                        {
                            aiDemerit[i] += (aiLength[i] - 2);
                        }
                        aiLastBitValue[i] = -1;
                        aiLength[i] = 1;
                    }
                }
                // 1“_–Ú‚ÌŒ¸“_ƒ`ƒFƒbƒN€–Ú
                // Y•ûŒü‚Ì5ƒrƒbƒgˆÈã‚Ì“¯’l˜A‘±.
                for (x = 0; x < m_iSize; x++)
                {
                    int[] aiLastBitValue = new int[8];
                    int[] aiLength = new int[8];
                    for (i = 0; i < 8; i++)
                    {
                        aiLastBitValue[i] = -1;
                        aiLength[i] = 0;
                    }
                    for (y = 0; y < m_iSize; y++)
                    {
                        for (i = 0; i < 8; i++)
                        {
                            if (aiLastBitValue[i] == -1)
                            {
                                aiLastBitValue[i] = ((m_ppbyteData[y, x] >> 1) & 1);
                                aiLength[i] = 1;
                            }
                            else if (aiLastBitValue[i] == ((m_ppbyteData[y, x] >> 1) & 1))
                            {
                                aiLength[i]++;
                            }
                            else
                            {
                                if (aiLength[i] >= 5)
                                {
                                    aiDemerit[i] += (aiLength[i] - 2);
                                }
                                aiLastBitValue[i] = ((m_ppbyteData[y, x] >> 1) & 1);
                                aiLength[i] = 1;
                            }
                        }
                    }
                    for (i = 0; i < 8; i++)
                    {
                        if (aiLength[i] >= 5)
                        {
                            aiDemerit[i] += (aiLength[i] - 2);
                        }
                        aiLastBitValue[i] = -1;
                        aiLength[i] = 1;
                    }
                }
                // 2“_–Ú‚ÌŒ¸“_ƒ`ƒFƒbƒN€–Ú
                // 2X2‚Ì“¯FƒuƒƒbƒN‚Ì‘¶Ý‚PŒÂ‚É‚Â‚«‚R“_Œ¸“_
                for (y = 1; y < m_iSize; y++)
                {
                    for (x = 1; x < m_iSize; x++)
                    {
                        int iAndValue = 0xFF;
                        iAndValue &= (m_ppbyteData[y - 0, x - 0]);
                        iAndValue &= (m_ppbyteData[y - 0, x - 1]);
                        iAndValue &= (m_ppbyteData[y - 1, x - 0]);
                        iAndValue &= (m_ppbyteData[y - 1, x - 1]);

                        int iOrValue = 0;
                        iOrValue |= (m_ppbyteData[y - 0, x - 0]);
                        iOrValue |= (m_ppbyteData[y - 0, x - 1]);
                        iOrValue |= (m_ppbyteData[y - 1, x - 0]);
                        iOrValue |= (m_ppbyteData[y - 1, x - 1]);
                        iOrValue &= 0xFF;

                        for (i = 0; i < 8; i++)
                        {
                            if (((iAndValue & (1 << i)) != 0) || ((iOrValue & (1 << i)) == 0))
                            {
                                aiDemerit[i] += 3;
                            }
                        }
                    }
                }
                // 3“_–Ú‚ÌŒ¸“_ƒ`ƒFƒbƒN€–Ú
                // X•ûŒü‚É 1,1,3,1,1‚Ìƒpƒ^[ƒ“‚ª‘¶Ý‚µ‚½‚ç40“_Œ¸“_
                for (y = 0; y < m_iSize; y++)
                {
                    int[] aiHistory = new int[8];
                    for (i = 0; i < 8; i++)
                    {
                        aiHistory[i] = 0;
                    }
                    for (x = 0; x < m_iSize; x++)
                    {
                        for (i = 0; i < 8; i++)
                        {
                            aiHistory[i] = ((aiHistory[i] & 0x3F) << 1) | ((m_ppbyteData[y, x] >> i) & 1);
                            if (aiHistory[i] == 0x5D)
                            {
                                aiDemerit[i] += 40;
                            }
                        }
                    }
                }
                // 3“_–Ú‚ÌŒ¸“_ƒ`ƒFƒbƒN€–Ú
                // Y•ûŒü‚É 1,1,3,1,1‚Ìƒpƒ^[ƒ“‚ª‘¶Ý‚µ‚½‚ç40“_Œ¸“_
                for (x = 0; x < m_iSize; x++)
                {
                    int[] aiHistory = new int[8];
                    for (i = 0; i < 8; i++)
                    {
                        aiHistory[i] = 0;
                    }
                    for (y = 0; y < m_iSize; y++)
                    {
                        for (i = 0; i < 8; i++)
                        {
                            aiHistory[i] = ((aiHistory[i] & 0x3F) << 1) | ((m_ppbyteData[y, x] >> i) & 1);
                            if (aiHistory[i] == 0x5D)
                            {
                                aiDemerit[i] += 40;
                            }
                        }
                    }
                }
                // 4ŒÂ–Ú‚ÌŒ¸“_ƒ`ƒFƒbƒN€–Ú.
                // 1‚Ìƒrƒbƒg”‚ð”‚¦‚é.
                int[] s_aiBitCounterToDemerit = new int[21] {
        90,80,70,60,50,40,30,20,10, 0,
         0,10,20,30,40,50,60,70,80,90,
        90
    };
                int[] aiSetBitCounter = new int[8];
                for (i = 0; i < 8; i++)
                {
                    aiSetBitCounter[i] = 0;
                }
                for (y = 0; y < m_iSize; y++)
                {
                    for (x = 0; x < m_iSize; x++)
                    {
                        for (i = 0; i < 8; i++)
                        {
                            if ((m_ppbyteData[y, x] & (1 << i)) != 0)
                            {
                                aiSetBitCounter[i]++;
                            }
                        }
                    }
                }
                for (i = 0; i < 8; i++)
                {
                    int iRatio = (aiSetBitCounter[i] * 20) / iTotalBitCount;
                    if (iRatio < 0)
                    {
                        iRatio = 0;
                    }
                    if (iRatio > 20)
                    {
                        iRatio = 20;
                    }
                    aiDemerit[i] += s_aiBitCounterToDemerit[iRatio];
                }
                int iMinValue = 0;
                int iRet = 0;
                for (i = 0; i < 8; i++)
                {
                    // ‚±‚ê‚Ü‚Å‚ÌÅ¬Ž¸“_‚æ‚è‚àŽ¸“_‚ª­‚È‚¯‚ê‚Î
                    // Å“Kƒ}ƒXƒN‚Ì’l‚ðXV‚·‚é.
                    if ((i == 0) || (aiDemerit[i] < iMinValue))
                    {
                        iRet = i;
                        iMinValue = aiDemerit[i];
                    }
                }
                return (iRet);
            }
        };

        // •¶Žš’·ŽwŽ¦Žq‚Ìƒrƒbƒg’·‚ðŒvŽZ‚·‚é.
        static int CalcBitLengthOfStringLengthSpecifier(int iEncodeType, int iVersion)
        {
            byte[,] s_auchStringLengthSpecifierBitLength = new byte[4, 3] {
           {10,12,14},{9,11,13},{8,16,16},{8,10,12}
            };
            /*  static unsigned char s_auchStringLengthSpecifierBitLength[4][3] = {
          {10,12,14},{9,11,13},{8,16,16},{8,10,12}
      };*/
            int iEncodeIndex = 0;
            switch (iEncodeType)
            {
                case Constants.ENCODEMODE_NUMERIC: iEncodeIndex = 0; break;
                case Constants.ENCODEMODE_ALPHANUMERIC: iEncodeIndex = 1; break;
                case Constants.ENCODEMODE_BINARY: iEncodeIndex = 2; break;
                case Constants.ENCODEMODE_KANJI: iEncodeIndex = 3; break;
                default: return 0;
            }
            int iVersionIndex = 0;
            if (iVersion > 40)
            {
                return 0;
            }
            else if (iVersion >= 27)
            {
                iVersionIndex = 2;
            }
            else if (iVersion >= 10)
            {
                iVersionIndex = 1;
            }
            if (iVersion > 0)
            {
                iVersionIndex = 0;
            }
            else
            {
                return 0;
            }
            return s_auchStringLengthSpecifierBitLength[iEncodeIndex, iVersionIndex];
        }

        // 
        static int CalcErrorCorrectionCodeBlockSizeInByte(int iVersion, int iEccType)
        {
            byte[,] s_auchErrorCorrectionCodeBlockSize = new byte[41, 4] {
        {  0,  0,  0,  0 },// 0
		{ 10,  7, 17, 13 },// 1
		{ 16, 10, 28, 22 },// 2
		{ 26, 15, 22, 18 },// 3
		{ 18, 20, 16, 26 },// 4
		{ 24, 26, 22, 18 },// 5
		{ 16, 18, 28, 24 },// 6
		{ 18, 20, 26, 18 },// 7
		{ 22, 24, 26, 22 },// 8
		{ 22, 30, 24, 20 },// 9
		{ 26, 18, 28, 24 },//10
		{ 30, 20, 24, 28 },//11
		{ 22, 24, 28, 26 },//12
		{ 22, 26, 22, 24 },//13
		{ 24, 30, 24, 20 },//14
		{ 24, 22, 24, 30 },//15
		{ 28, 24, 30, 24 },//16
		{ 28, 28, 28, 28 },//17
		{ 26, 30, 28, 28 },//18
		{ 26, 28, 26, 26 },//19
		{ 26, 28, 28, 30 },//20
		{ 26, 28, 30, 28 },//21
		{ 28, 28, 24, 30 },//22
		{ 28, 30, 30, 30 },//23
		{ 28, 30, 30, 30 },//24
		{ 28, 26, 30, 30 },//25
		{ 28, 28, 30, 28 },//26
		{ 28, 30, 30, 30 },//27
		
		{ 28, 30, 30, 30 },//28
		{ 28, 30, 30, 30 },//29
		{ 28, 30, 30, 30 },//30
		{ 28, 30, 30, 30 },//31
		{ 28, 30, 30, 30 },//32
		{ 28, 30, 30, 30 },//33
		{ 28, 30, 30, 30 },//34
		{ 28, 30, 30, 30 },//35
		{ 28, 30, 30, 30 },//36
		{ 28, 30, 30, 30 },//37
		{ 28, 30, 30, 30 },//38
		{ 28, 30, 30, 30 },//39
		{ 28, 30, 30, 30 } //40
	};
            if (iVersion <= 0)
            {
                return 0;
            }
            if (iVersion > 40)
            {
                return 0;
            }
            int iEccTypeIndex = 0;
            switch (iEccType)
            {
                case Constants.ECCTYPE_M: iEccTypeIndex = 0; break;
                case Constants.ECCTYPE_L: iEccTypeIndex = 1; break;
                case Constants.ECCTYPE_H: iEccTypeIndex = 2; break;
                case Constants.ECCTYPE_Q: iEccTypeIndex = 3; break;
                default: return 0;
            }
            return s_auchErrorCorrectionCodeBlockSize[iVersion, iEccTypeIndex];
        }

        public static byte[,,] s_aaachBlockInfo = new byte[41, 4, 3]{
    {{   0,   0,   0 }, {   0,   0,   0 }, {   0,   0,   0 }, {   0,   0,   0 }}, // 0
	{{  26,   1,   0 }, {  26,   1,   0 }, {  26,   1,   0 }, {  26,   1,   0 }}, // 1
	{{  44,   1,   0 }, {  44,   1,   0 }, {  44,   1,   0 }, {  44,   1,   0 }}, // 2
	{{  70,   1,   0 }, {  70,   1,   0 }, {  35,   2,   0 }, {  35,   2,   0 }}, // 3
	{{  50,   2,   0 }, { 100,   1,   0 }, {  25,   4,   0 }, {  50,   2,   0 }}, // 4
	{{  67,   2,   0 }, { 134,   1,   0 }, {  33,   2,   2 }, {  33,   2,   2 }}, // 5
	{{  43,   4,   0 }, {  86,   2,   0 }, {  43,   4,   0 }, {  43,   4,   0 }}, // 6
	{{  49,   4,   0 }, {  98,   2,   0 }, {  39,   4,   1 }, {  32,   2,   4 }}, // 7
	{{  60,   2,   2 }, { 121,   2,   0 }, {  40,   4,   2 }, {  40,   4,   2 }}, // 8
	{{  58,   3,   2 }, { 146,   2,   0 }, {  36,   4,   4 }, {  36,   4,   4 }}, // 9
	{{  69,   4,   1 }, {  86,   2,   2 }, {  43,   6,   2 }, {  43,   6,   2 }}, // 10
	{{  80,   1,   4 }, { 101,   4,   0 }, {  36,   3,   8 }, {  50,   4,   4 }}, // 11
	{{  58,   6,   2 }, { 116,   2,   2 }, {  42,   7,   4 }, {  46,   4,   6 }}, // 12
	{{  59,   8,   1 }, { 133,   4,   0 }, {  33,  12,   4 }, {  44,   8,   4 }}, // 13
	{{  64,   4,   5 }, { 145,   3,   1 }, {  36,  11,   5 }, {  36,  11,   5 }}, // 14
	{{  65,   5,   5 }, { 109,   5,   1 }, {  36,  11,   7 }, {  54,   5,   7 }}, // 15
	{{  73,   7,   3 }, { 122,   5,   1 }, {  45,   3,  13 }, {  43,  15,   2 }}, // 16
	{{  74,  10,   1 }, { 135,   1,   5 }, {  42,   2,  17 }, {  50,   1,  15 }}, // 17
	{{  69,   9,   4 }, { 150,   5,   1 }, {  42,   2,  19 }, {  50,  17,   1 }}, // 18
	{{  70,   3,  11 }, { 141,   3,   4 }, {  39,   9,  16 }, {  47,  17,   4 }}, // 19
	{{  67,   3,  13 }, { 135,   3,   5 }, {  43,  15,  10 }, {  54,  15,   5 }}, // 20
	{{  68,  17,   0 }, { 144,   4,   4 }, {  46,  19,   6 }, {  50,  17,   6 }}, // 21
	{{  74,  17,   0 }, { 139,   2,   7 }, {  37,  34,   0 }, {  54,   7,  16 }}, // 22
	{{  75,   4,  14 }, { 151,   4,   5 }, {  45,  16,  14 }, {  54,  11,  14 }}, // 23
	{{  73,   6,  14 }, { 147,   6,   4 }, {  46,  30,   2 }, {  54,  11,  16 }}, // 24
	{{  75,   8,  13 }, { 132,   8,   4 }, {  45,  22,  13 }, {  54,   7,  22 }}, // 25
	{{  74,  19,   4 }, { 142,  10,   2 }, {  46,  33,   4 }, {  50,  28,   6 }}, // 26
	{{  73,  22,   3 }, { 152,   8,   4 }, {  45,  12,  28 }, {  53,   8,  26 }}, // 27
	{{  73,   3,  23 }, { 147,   3,  10 }, {  45,  11,  31 }, {  54,   4,  31 }}, // 28
	{{  73,  21,   7 }, { 146,   7,   7 }, {  45,  19,  26 }, {  53,   1,  37 }}, // 29
	{{  75,  19,  10 }, { 145,   5,  10 }, {  45,  23,  25 }, {  54,  15,  25 }}, // 30
	{{  74,   2,  29 }, { 145,  13,   3 }, {  45,  23,  28 }, {  54,  42,   1 }}, // 31
	{{  74,  10,  23 }, { 145,  17,   0 }, {  45,  19,  35 }, {  54,  10,  35 }}, // 32
	{{  74,  14,  21 }, { 145,  17,   1 }, {  45,  11,  46 }, {  54,  29,  19 }}, // 33
	{{  74,  14,  23 }, { 145,  13,   6 }, {  46,  59,   1 }, {  54,  44,   7 }}, // 34
	{{  75,  12,  26 }, { 151,  12,   7 }, {  45,  22,  41 }, {  54,  39,  14 }}, // 35
	{{  75,   6,  34 }, { 151,   6,  14 }, {  45,   2,  64 }, {  54,  46,  10 }}, // 36
	{{  74,  29,  14 }, { 152,  17,   4 }, {  45,  24,  46 }, {  54,  49,  10 }}, // 37
	{{  74,  13,  32 }, { 152,   4,  18 }, {  45,  42,  32 }, {  54,  48,  14 }}, // 38
	{{  75,  40,   7 }, { 147,  20,   4 }, {  45,  10,  67 }, {  54,  43,  22 }}, // 39
	{{  75,  18,  31 }, { 148,  19,   6 }, {  45,  20,  61 }, {  54,  34,  34 }}, // 40
};

        static int CalcTotalBlockSizeInByte(int iVersion, int iEccType)
        {
            if (iVersion <= 0)
            {
                return 0;
            }
            if (iVersion > 40)
            {
                return 0;
            }
            int iEccTypeIndex = 0;
            switch (iEccType)
            {
                case Constants.ECCTYPE_M: iEccTypeIndex = 0; break;
                case Constants.ECCTYPE_L: iEccTypeIndex = 1; break;
                case Constants.ECCTYPE_H: iEccTypeIndex = 2; break;
                case Constants.ECCTYPE_Q: iEccTypeIndex = 3; break;
                default: return 0;
            }
            return s_aaachBlockInfo[iVersion, iEccTypeIndex, 0];
        }

        static int CalcBlockCount1(int iVersion, int iEccType)
        {
            if (iVersion <= 0)
            {
                return 0;
            }
            if (iVersion > 40)
            {
                return 0;
            }
            int iEccTypeIndex = 0;
            switch (iEccType)
            {
                case Constants.ECCTYPE_M: iEccTypeIndex = 0; break;
                case Constants.ECCTYPE_L: iEccTypeIndex = 1; break;
                case Constants.ECCTYPE_H: iEccTypeIndex = 2; break;
                case Constants.ECCTYPE_Q: iEccTypeIndex = 3; break;
                default: return 0;
            }
            return s_aaachBlockInfo[iVersion, iEccTypeIndex, 1];
        }

        static int CalcBlockCount2(int iVersion, int iEccType)
        {
            if (iVersion <= 0)
            {
                return 0;
            }
            if (iVersion > 40)
            {
                return 0;
            }
            int iEccTypeIndex = 0;
            switch (iEccType)
            {
                case Constants.ECCTYPE_M: iEccTypeIndex = 0; break;
                case Constants.ECCTYPE_L: iEccTypeIndex = 1; break;
                case Constants.ECCTYPE_H: iEccTypeIndex = 2; break;
                case Constants.ECCTYPE_Q: iEccTypeIndex = 3; break;
                default: return 0;
            }
            return s_aaachBlockInfo[iVersion, iEccTypeIndex, 2];
        }

        // Ši”[‰Â”\‚Èƒf[ƒ^‚ÌãŒÀ—Ê‚ðƒoƒCƒg”‚Å•Ô‚·.
        static int CalcMaxDataSizeInByte(int iVersion, int iEccType)
        {
            int iDataBlockSizeInByte = CalcTotalBlockSizeInByte(iVersion, iEccType) - CalcErrorCorrectionCodeBlockSizeInByte(iVersion, iEccType);
            int iTotalDataSizeInByte = iDataBlockSizeInByte * CalcBlockCount1(iVersion, iEccType) +
                    (iDataBlockSizeInByte + 1) * CalcBlockCount2(iVersion, iEccType);
            return iTotalDataSizeInByte;
        }

        // Ši”[‰Â”\‚Èƒf[ƒ^‚ÌãŒÀ—Ê‚ðƒrƒbƒg”‚Å•Ô‚·.
        public static int CalcMaxDataSizeInBit(int iVersion, int iEccType)
        {
            return (CalcMaxDataSizeInByte(iVersion, iEccType) * 8);
        }

        // Ši”[‰Â”\‚Èƒf[ƒ^‚ÌãŒÀ—Ê‚ðAƒwƒbƒ_•”•ª‚ðœ‚¢‚ÄAƒrƒbƒg”‚Å•Ô‚·.
        public static int CalcMaxDataSizeExceptHeaderPartInBit(int iEncodeType, int iVersion, int iEccType)
        {
            return (CalcMaxDataSizeInBit(iVersion, iEccType) - (4 + CalcBitLengthOfStringLengthSpecifier(iEncodeType, iVersion)));
        }

        // ƒŠ[ƒhƒ\ƒƒ‚ƒ“•„†‚Ì¶¬‘½€Ž®

        public int CreateQRCodeBitmap(CQRCodeBitmap QRCodeBitmap, int iEncodeMode, int iVersion, int iEccType, byte[] pchData)
        {
            CVariableLengthDataWordArray VariableLengthDataWordArray = new CVariableLengthDataWordArray();
            if (VariableLengthDataWordArray.SetData(iEncodeMode, iVersion, iEccType, pchData, /*strlen(pchData)*/pchData.Length) <= 0)
            {
                return -1;
            }
            CFixedLengthDataWordArray FixedLengthDataWordArray = new CFixedLengthDataWordArray();
            if (FixedLengthDataWordArray.Merge(VariableLengthDataWordArray) <= 0)
            {
                return -1;
            }
            CErrorCorrectionCodeArray ErrorCorrectionCodeArray = new CErrorCorrectionCodeArray();
            if (ErrorCorrectionCodeArray.Setup(FixedLengthDataWordArray) <= 0)
            {
                return -1;
            }
            CInterleavedCodeArray InterleavedCodeArray = new CInterleavedCodeArray();
            if (InterleavedCodeArray.Setup(FixedLengthDataWordArray, ErrorCorrectionCodeArray) <= 0)
            {
                return -1;
            }
            CQRCodeBitLocator QRCodeBitLocator = new CQRCodeBitLocator();
            if (QRCodeBitLocator.Setup(InterleavedCodeArray.GetVersion()) <= 0)
            {
                // this is the place need to check - meena
                return -1;
            }
            if (QRCodeBitLocator.LocateData(QRCodeBitmap, InterleavedCodeArray, 1) <= 0)
            {
                return -1;
            }
            int iBestMask = QRCodeBitmap.CalcBestMask(QRCodeBitLocator.CountTotalDataBit());
            if (QRCodeBitLocator.LocateData(QRCodeBitmap, InterleavedCodeArray, 0) <= 0)
            {
                return -1;
            }
            return iBestMask;
        }

    }
}
